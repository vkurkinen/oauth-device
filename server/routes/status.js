var log4js      = require('log4js');

var metrics     = require('../lib/metrics');

/*
 * Provides system status and health information.
 *
 */
exports.health = function(req, res){
  var log           = log4js.getLogger("token");
  var auth_service  = req.app.get("auth_service");

  // this is really healthy for now
  log.info("status: health OK");
    res.send(200,"OK");
};

exports.metrics = function(req, res){
  var log           = log4js.getLogger("token");
  var auth_service  = req.app.get("auth_service");
  res.json(metrics.getCommonCollection());
};

exports.idp_metrics = function(req, res){
  var log  = log4js.getLogger("token");
  var idp  = req.idp;
  res.json(metrics.getIdPCollection(idp.key));
};