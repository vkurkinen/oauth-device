
/*
 * Handles the default request to the user UI part.
 * 
 */
exports.index = function(req, res){
  res.redirect('./user');
};