var querystring = require("querystring");
var request     = require('request');
var i18n        = require("i18next");
var log4js      = require('log4js');
/*
 * User UI endpoints_
 * - "code"    :  the main entry, asks for user code
 * - "callback" :  Callback from authz
 */
exports.index = function(req, res){
	model            = {}
	model.ns         = req.ns;
	res.render('user_code', model);
};

exports.code = function(req, res){
	var log          = log4js.getLogger("user/code");
	var auth_service = req.app.get("auth_service");	
	var user_code    = req.body.code || req.query.code;

	auth_service.processUserCodeRequest(req.idp, user_code, function(error, entry, state) {		
		if (error) {
			res.render('user_code', { error: error,  user_code: user_code, ns: req.ns });
			return;
		}		
		
		// else, trigger oauth authz code grant flow with the IdP.

		var q = 'redirect_uri=' + querystring.escape(req.idp.callback_uri(req)) +
		        '&response_type=code' + 
		        '&scope=' + querystring.escape(entry.scope) +
		        '&client_id=' + querystring.escape(entry.client_id) +
		        '&state=' + state;
		var authz_url = req.idp.authorize_uri(req);
		if (authz_url.indexOf('?') > 0) {
			authz_url += '&' + q;
		} else {
			authz_url += '?' + q;
		}
		
		log.info("redirecting user to idp " + entry.idp_key + " -> " + authz_url);

		// update locale to entry to ensure it is correct.
    	entry.locale = req.locale;    	
		auth_service.entryInProgress(entry);
		res.redirect(authz_url);
	});
};

/**
* Process OAuth2 callback from AS . If successful, exchange authz code to 
* tokens and store them to device entry. 
* 
* If errors, store error to the device entry so that it can be then 
* delivered to the device.
*/
exports.callback = function(req, res){
	var log = log4js.getLogger("user/cb");    
	log.info("received authz callback: " + JSON.stringify(req.query));	
	var auth_service = req.app.get("auth_service");	

	auth_service.processAuthzCallbackRequest(req.idp, req.query, req, function(error, entry) {				
		var model = {};	
		model.ns  = req.ns;
		if (error != null) {
			log.warn("no entry for user code exist anymore, maybe expired?");
			model.error = error
			model.message = error
			res.render('user_callback', model);
			return;
		}
		model.success    = true;
		model.message    = 'auth_success';
		res.render('user_callback', model);		
	});	
};
		
