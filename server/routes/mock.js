/**
* Implements simple mock OAuth2 AS.
*
* Currently:
*   * Authorization code flow 
*   * Refresh token
*   * OpenID Connect 1.0 UserInfo formatted dummy data.
* 
*
*/
var log  = require("log4js").getLogger("mock");
var util = require("../lib/util");

// process new device code request.
exports.authz = function(req, res){
	var mock = req.app.get('mock_as');
	var response_type = req.query.response_type || req.body.response_type;   
  	var idp           = req.idp;
  	if (response_type == 'code') {  
    	if (!req.client_id) {
        	util.error(res,'invalid_request', 'client_id must be provided');
        	return;
    	}  
		handle_authz_code_req(req,res,mock, idp, log);
		return;
  	} 	  
  	util.error(res, 'invalid_request', 'unsupported response type');  
};

/**
* Process device token poll, refresh token or PF token validation request. 
*/
exports.token = function(req, res){
	var mock 		= req.app.get('mock_as');
    var idp         = req.idp;  
    var grant_type  = req.query.grant_type || req.body.grant_type;  
    log.info("recvd token req with grant type " + grant_type);
    
    switch (grant_type) {
  		case 'authorization_code':  			
	  		handle_code_token_req(req, res, mock, idp, log);
  			break;
  		case 'refresh_token':  			
	  		handle_refresh_token_req(req, res, mock, idp, log);	
	  		break;
	  	case 'urn:mock:oauth2:grant_type:validate_bearer':
	  		handle_pf_validate_token_req(req, res, mock, idp, log);
	  		break;
	  	default:
			util.error(res, 'invalid_request','unknown grant_type');	  	
    } 
};

exports.profile = function(req, res){
	var mock 		= req.app.get('mock_as');
    var idp         = req.idp;  
    log.info("recvd profile request");
  	handle_profile_req(req, res, mock, idp, log); 
};

/**
* Perform authorization request. As this is mock AS, it almost always succeeds
* for any scope and redirect_uri.
*/
function handle_authz_code_req(req, res, mock, idp, log) {
	var state        = req.query.state || req.body.state;
	var scope        = req.query.scope || req.body.scope;	
	var redirect_uri = req.query.redirect_uri || req.body.redirect_uri;
    var entry 		 = mock.authz(req.client_id, scope);	
	
    var url = redirect_uri;
	url += ((url.indexOf("?") < 0) ? "?" : "&");	
	url += "code=" + entry.authz_code;
	if (state) {
		url += "&state=" + state;
	}
	res.redirect(url);
}

/**
* Exchange authorization code to access/refresh token pair.
*/
function handle_code_token_req(req, res, mock, idp, log) {	
	var code = req.query.code || req.body.code;
	if (!code) {
    	util.error(res, 'invalid_request','authorization code must be provided in request');
	   	return;
	}		
	// otherwise, process exchange	  
	mock.code_token(code, req.client_id, req.client_secret, function(err, token) {		  
		if (token == null) {	
			log.warn("no token matching provided code (" + err + ")");
			util.error(res, 'invalid_grant','unknown authorization code');
			return;
		}		  
		// else code is valid and tokens are there.
		var returned_token = {
			access_token : token.access_token,
			token_type   : token.token_type
		};
		if (token.refresh_token) {			
			returned_token.refresh_token = token.refresh_token;			
		}
		util.response(res, returned_token);
	});
}

/**
* Perform token refresh with existing refresh token. This creates
* new token pair if the current token entry still exists in cache.
*/
function handle_refresh_token_req(req, res, mock, idp, log) {
	var scope         = req.query.scope || req.body.scope;
	var refresh_token = req.query.refresh_token || req.body.refresh_token;	

	if (!refresh_token) {		  	  
	    util.error(res, 'invalid_request','refresh_token is required');
	    return;
	}		  	  
	
	mock.refresh_token(refresh_token, scope, req.client_id, req.client_secret, function (err, token){
		if (token == null) {	
		    log.warn("no token matching provided code (" + err + ")");
			util.error(res, 'invalid_grant','unknown authorization code');			
			return;
		}		  
		// else code is valid and tokens are there.
		util.response(res, token);
	});	
}

/**
* Perform user profile request.
*/
function handle_profile_req(req, res, mock, idp, log) {
	var access_token = req.access_token;
	if (!access_token) {		  	  
	   util.error(res,'invalid_request','Bearer access_token is required');
	   return;
	}	
	log.debug("fetching user profile for token " + access_token);
	mock.openid_connect_userinfo(access_token, req.client_id, req.client_secret, function (error, userinfo){
		  if (error) {	
			  log.warn("no token matching provided code (" + error + ")");
			  util.error(res, error, 'invalid access token');
			  return;
		  }		  
		  // else code is valid and tokens are there.
		  log.info("returning user data -> " + JSON.stringify(userinfo));
		  util.response(res, userinfo);
	});	
}
