/**
* Implements OAuth2 Device Code flow kind of
* delegated authorization for apps that cannot request
* user input in a convenient way.
*
* Such devices can request an authorization transaction
* from this API and get a pair of codes that are
* used to manage delegated user authorization flow
* with an external AS / IdP and to deliver the authorization
* response (error or tokens) to the device once user is done with
* the authorization (which is performed in a remote IdP).
*/
var querystring = require("querystring");
var request     = require('request');
var log4js      = require('log4js');
var util        = require('../lib/util');

/**
* Process new device code request.
* Generates device and user code and returns a URL where user
* needs to go for completing the authorization.
*
* While user is in doing authorization, the device polls
* the token endpoint of this API to get information how the
* authorization is going and finally the result once user
* has completed authorization (or the code expires).
*/
exports.code = function(req, res){
    var log           = log4js.getLogger("token");
    var auth_service  = req.app.get("auth_service");
    var response_type = req.query.response_type || req.body.response_type;
    // only device_code response is supported for now.
    if (response_type == 'device_code') {
	    handle_device_code_req(req,res,auth_service, log);
	    return;
    }

	idp.stats.deviceCodeInvalidRequest.mark();
	this.stats.deviceCodeInvalidRequest.mark();

    util.error(res, 'invalid_request', 'unsupported response type');
};

/**
* Process device token poll or refresh token request.
*/
exports.token = function(req, res){
	var log           = log4js.getLogger("token");
	var auth_service  = req.app.get("auth_service");
	var grant_type      = req.query.grant_type || req.body.grant_type;

	switch (grant_type) {
		case 'device_code':
	    	handle_device_token_req(req, res, auth_service, log);
	    	break;
	    case 'refresh_token':
	    	handle_refresh_token_req(req, res, auth_service, log);
			break;
		default:
			util.error(res, 'invalid_request','unknown grant_type');
	}
};


function handle_device_code_req(req, res, auth_service, log) {
	var scope = req.query.scope || req.body.scope;
    var now = new Date().getTime();

    if (!req.client_id) {
		util.error(res, 'invalid_request','missing client_id');
		return;
    }

	var entry = auth_service.newDeviceEntry(req.idp, req.ns, req.locale, req.client_id, req.client_secret, scope);
    req.idp.verification_uri(req, function (verification_uri) {
		var response = {
			device_code      : entry.device_code,
			user_code        : entry.user_code,
	        verification_uri : verification_uri,
	        expires_in       : Math.floor((entry.expiry_time - now) / 1000),
	        interval         : entry.interval
	    };
		util.response(res, response);
    });

}

function handle_device_token_req(req, res, auth_service, log) {
	var device_code = req.query.device_code || req.body.device_code;
	if (!device_code || device_code == null) {
	   util.error(res, 'invalid_request','device code must be provided in request');
	   return;
	}
	// otherwise, poll status
	auth_service.processDevicePollRequest(req.idp, device_code, function(error, entry) {
		if (error) {
		    util.response(res, error);
	   	    return;
		}

		var response = {};
		// else the entry has completed.

		// TODO: move logic to service module.
		if (entry.hasTokens()) {
		    // tokens are returned only if not yet expired.
		    var tokens = entry.getTokens();
		 	if (typeof tokens.expires_in == undefined || tokens.expires_in == null || tokens.expires_in > 0) {
				// TODO: make deep clone of tokens
				response = tokens;
				if (!response.issuer) {
					response.issuer = entry.issuer;
				}
			} else {
			   response.error = "invalid_grant";
			   response.error_description = "token has expired";
			};
		} else {
		    response = entry.getError();
	    }
	    util.response(res, response);
	  });
}

function handle_refresh_token_req(req, res, auth_service, log) {

	// TODO: move to service.
	var refresh_token = req.query.refresh_token || req.body.refresh_token;
	var scope = req.query.scope || req.body.scope;

	if (!refresh_token) {
	   util.error(res, 'invalid_request','refresh_token is required');
	   return;
	}

	var body = {
		grant_type    : 'refresh_token',
		refresh_token : refresh_token,
		redirect_uri  : req.idp.callback_uri(req)
	    };

	if (scope) {
	    body.scope = scope;
	}

	// TODO: make client creds method configurable in idp (basic,form,query)
    var options = { followRedirect: true, form : body };
    if (req.client_id) {
       options.auth = { user: req.client_id,  pass: req.client_secret, sendImmediately: true };
    }

    var idpTokenRefreshTimer = idp.stats.tokenRefresh.start();
    var tokenRefreshTimer   = auth_service.stats.tokenRefresh.start();

    request.post(idp.token_uri(req), options, function(err, resp, data) {
    	idpTokenRefreshTimer.end();
    	tokenRefreshTimer.end();
		try {
		 	log.info("received refresh response: " + JSON.stringify(data));
			if (data) {
		  		data = JSON.parse(data);
		  	} else {
				data = {
			  	 	error: 'temporarily_unavailable',
			   		error_description: 'malformed data received from remote authorization server'
				};
		  	}
		} catch (e) {
		  log.warn("remote AS sent malformed response to token request: " + e);
		  data = {
			error: 'temporarily_unavailable',
			error_description: 'malformed data received from remote authorization server: ' + e.message
		  };
	    }
	    // TODO: make deep clone of data
	    if (data.access_token && !data.issuer) {
			data.issuer = req.idp.issuer();
	    }

	    // update error stats
	    if (!data.access_token) {
	    	idp.stats.tokenRefreshError.mark();
	      	auth_service.stats.tokenRefreshError.mark();
	    }
		util.response(res,data);
  	});
}
