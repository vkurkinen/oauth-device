#!/bin/bash

#
# Simple release, this should be updated to be done via grunt                                                                                                                            # and take meta data from package.json

# 
VERSION="$1"  
NAME="oauth2-device-server"
PREFIX="/opt/app/oauth2dev/rel/server"
USER="oauth2dev"
GROUP="oauth2dev"
MAINTAINER="Ville Kurkinen <ville@kurkinen.net>"
SHORT_DESCRIPTION="OAuth2 Device Authorization Gateway Server"

fpm -s dir -t deb \
     --force \
     --prefix $PREFIX \
     --description "$SHORT_DESCRIPTION" \
     --deb-user $USER \
     --deb-group $GROUP \
     -a all \
     -n $NAME \
     -v $VERSION \
     -p ./build \
     -m "$MAINTAINER" \
      -x \*~ \
     -x conf \
     -x build \
     -x release.sh \
     .