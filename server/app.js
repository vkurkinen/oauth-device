
/**
 * Main Device OAuth Gateway app.
 */

 // main module deps
var express = require('express');
var routes  = require('./routes');
var http    = require('http');
var path    = require('path');
var cors    = require('./lib/cors');
var i18next = require('i18next');
require('enum').register();

// routes
var token  = require('./routes/token');
var user   = require('./routes/user');
var status = require('./routes/status');
var mock   = require('./routes/mock');

// configure logging
var log4js = require('log4js');
log4js.configure('/etc/oauth2dev.d/log4js.json', { reloadSecs: 5 });

// create main module instances
var log              = log4js.getLogger('app');
var configuration    = require('./lib/config');
var middleware       = require('./lib/middleware');

// setup audit log
var AuditLog = require('./lib/audit_log');
var audit_log = new AuditLog(configuration.get('audit_log_file'), configuration.get('audit_log_period'), configuration.get('audit_log_count'));

var OAuth2DeviceAuth = require('./lib/oauth2_device_auth');
var auth_service     = new OAuth2DeviceAuth(configuration, audit_log);

var OAuth2MockAS = require('./lib/mock_as');
var mock_as      = new OAuth2MockAS(configuration, audit_log);

cors.configure(configuration);

// setup express, common middleware and routing
var app = express();

module.exports = app;
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.set('trust proxy', 'loopback');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

// set modules to app to be avaialable where needed.
app.set('auth_service', auth_service);
app.set('audit_log', audit_log);
app.set('mock_as', mock_as);

app.webroot_ux = "/oauth/device/user/ux"
app.webroot_ui = "/oauth/u"
app.locals.webroot_ux = app.webroot_ux;

// localization / i18n support
var i18next_options = {
	 lng                  : 'en',
	 supportedLngs        : ['en-US','en', 'fi'],
	 detectLngFromPath    : 2,
	 detectLngQS          : 'lang',
	 cookieName           : 'lang',
	 useCookie            : true,
	 detectLngFromHeaders : true,
	 fallbackLng          :  'en',
	 resGetPath           : 'locales/__ns__-__lng__.json',
	 ns			          : 'messages',
	 ignoreRoutes		  : ['status/','images/', 'public/', 'css/', 'js/', 'fonts']
};

i18next.init(i18next_options);
i18next.registerAppHelper(app);

// this must be before i18next
app.use(middleware.lang2req);

app.use(i18next.handle);
// app middleware
app.use(middleware.clientCredentials);
app.param('ns_key', middleware.ns2req);
app.param('idp_context_key', middleware.idp2req);

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// all common request filters
var filters = [ cors.handler() ];

// preflight CORS is not needed by his app.
app.options('*', cors.handler());

// Routing
app.get('/', routes.index);

// status and health checks
app.get('/oauth/device/status/health'                    , filters , status.health);
app.get('/oauth/device/status/metrics'                   , filters , status.metrics);
app.get('/oauth/device/status/:idp_context_key/metrics'  , filters , status.idp_metrics);

// UI
app.get('/oauth/u/:lng/:ns_key/:idp_context_key/user'    , filters , user.code);
app.post('/oauth/u/:lng/:ns_key/:idp_context_key/user'   , filters , user.code);
app.get('/oauth/u/:lng/:ns_key/:idp_context_key/user/cb' , filters , user.callback);
app.get('/oauth/u/:ns_key/:idp_context_key/user'         , filters , user.code);
app.post('/oauth/u/:ns_key/:idp_context_key/user'        , filters , user.code);
app.get('/oauth/u/:ns_key/:idp_context_key/user/cb'      , filters , user.callback);
// API
app.post('/oauth/device/:ns_key/:idp_context_key/code'   , filters  , token.code);
app.post('/oauth/device/:ns_key/:idp_context_key/token'  , filters  , token.token);
// Mock AS used for offline development
app.get('/oauth/as/:ns_key/:idp_context_key/authz'       , filters  , mock.authz);
app.get('/oauth/as/:lng/:ns_key/:idp_context_key/authz'  , filters  , mock.authz);
app.post('/oauth/as/:ns_key/:idp_context_key/token'      , filters  , mock.token);
app.get('/oauth/as/:ns_key/:idp_context_key/profile'     , filters  , middleware.access_token2req, mock.profile);

// start server
http.createServer(app).listen(app.get('port'), function(){
  log.info('oauth2 device auth gateway listening on port ' + app.get('port'));
});
