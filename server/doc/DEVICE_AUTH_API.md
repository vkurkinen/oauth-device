Device Authorization API
========================

This chapter explains the API that device application uses in its backend for 
performing authorization, obtaining access (and optionally refresh) token and for 
refreshing the refresh token from the real authorization server. 

Overview
--------

The API is HTTP based closely following the standard [OAuth2](http://tools.ietf.org/html/rfc6749)
model with some modifications. 

The device application needs to communicate directly only with the gateway regardless which
authorization server is used. 

The gateway provides only link between application and authorization server and any 
communication with the resource server the access token is issued for is done 
directly from the application to the resource server. The gateway does not know which 
resource servers will be accessed with the access token.

This applies also for fetching the user profile data, in case the authorization server 
provides an API for that.

Also, if the authorization server sets any limits for the authorization scopes for the 
application, those need to be also allowed as the gateway merely acts as a middle man: 
all information needed by the authorization server related to OAuth2 flows are passsed 
from the application to the gateway, and from the gateway to the authorization server.

Gateway supports multiple authorization servers to be used from single instance and new 
servers can be added to its configuration. The authorization server to be used is 
requested by the application as part of the gateway URL so the application 
needs to know which server it wants to use.
 
About Client Credentials & Redirect URIs
----------------------------------------

The gateway does not maintain its own client credentials. Each application connecting to 
this gateway must have client credentials registered with the authorization server before 
they can utilize this flow. The credentials are stored temporarily in the gateway while 
it communicates with the authorization server. 

Before using the API, register your applications to the authorization server you want to use. 
Then read on how to integrate.

The operations in this API that require client credentials accept them in any of the 
following ways (in preferred order):

1. In Authorization header using HTTP Basic authentication.
2. In HTTP POST body as form parameters
3. In HTTP query parameters. 

The gateway utilizes OAuth2 *Authorization Code Grant* flow and the `redirect_uri`
used with this flow will point to the gateways callback endpoint. That URL must
be allowed to be used with your client credentials so it probably requires that
to be registered to the authorization server. 

The user callback URL used as `redirect_uri` is:

    http://<host>:<port>/oauth/u/<ns>/<as-id>/user/cb
    
Ensure that URLs mathing the above pattern are in the allowed redirect URLs in the authorization 
server. 

**NOTE**: The namespace part may depend on system configuration to there can be multiple different redirect URLs needed in authorization server white list (unless you can white list based on URL prefix).

API Endpoints
-------------

The gateway provides following URLs to be used: 

    http://<host>/oauth/device/<ns>/<as-id>/code
    http://<host>/oauth/device/<ns>/<as-id>/token

`<as-id>` is the identifier of the authorization server. It is registered in the gateway's configuration.

`<ns>` is the namespace identifier that tells the gateway which specific tenant or other system level 
configuration to apply when processing the request and the rest of the authorization process. Mostly
this is used to detect the host name part in user facing URLs and UI theming.
There can be many namespaces supported, value `d` should be usable always.

The examples below use value **mock** as the authorization server identifier which is suitable for 
quick testing as the gateway includes a built-in dummy mock authorization server.

**code** endpoint is used to initiate a new authorization transaction. 

**token** endpoint is used to poll status of an ongoing authorization request, to retrieve access 
token, and to refresh existing tokens.

Requesting Authorization (`/oauth/device/<ns>/<as-id>/code`)
-------------------------------------------------------

In order to utilize specific authorization server, it needs to be registered and configured to the gateway.
That is done as a separate task so lets assume your authorization server 
has been registered and is supported by the gateway (**mock** in below examples). 

After you have registered your application in the authorization server you intend to use and 
gained client credentials (and allowed gateways user callback URI to be used as redirect URL), 
you can use it to request user authorization for your application. 

Following parameters are used with this operation:

Parameter        |  Description
:--------------- | :-----------------------
`response_type`  | Tells which kind of code is being requested. With this flow always use value *device_code*.
`scope`          | Contains URL encoded, space separated list of authorization scopes that your application is requesting consent from the user. It is passed as is to the authorization server.
`lang`           | Optional language code to be used as hint for localization of the user facing screens. If this is not
provided, the user screens are rendered according to the language detected from the users browser. 

**Authorization** header contains your client credentials in form of **HTTP Basic** authentication.

Make following kind of request to the gateway to initiate new authorization process:

    POST /oauth/device/d/mock/code HTTP/1.1
    Host: localhost
    Authorization : Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
    Accept: application/json
    Content-Type: application/x-www-form-urlencoded

    response_type=device_code&scope=email%20profile&lang=en


Successful response is a **HTTP 200 OK** with JSON body: 

    HTTP 200 OK
    Content-Type: application/json

    {
		"device_code"      : "S2UJalLczDNixQrVsDmdbm4LEXTjYwc2",
		"user_code"        : "503893",
		"verification_uri" : "http://localhost:3000/oauth/u/en/d/mock/user",
		"expires_in"	   : 300,
		"interval"         : 10
	}

`device_code` is the identifier of the newly created authorization process that you will use later to query for access tokens. 

`user_code` is used to identify correct authorization transaction so that user will be authorizing correct
application in the authorization server. User needs to enter this code in the form provided from the `verification_uri`.
User codes are for one time use only, once a correct code has been entered in the verification 
URI, it cannot be used anymore to initiate another authorization user flow.

`verification_uri` is the URL where user should go to perform the actual authentication and authorization of your application. Show the URL to the user and request user to open the URL with her other device. **Do not** assume anything about the format of the URL, it may change at any time as it is strictly to be interpreted only by the gateway.
The URI accepts also HTTP GET so the user code and verification URI can be combined into single HTTP GET request for immediate use if so desired (e.g. as embedded into a QR code). 

`expires_in` tells the time, in seconds, until the current the transaction expires. If user does not complete authorization process within this time, the transaction expires and both the related device and user code 
become invalid.

`interval` tells the shortest allowed time, in seconds, between two subsequent checks to the device token endpoint 
for checking the status of the authorization process and attempt to obtain the authorization tokens. This is used for flow control, if you call the toke endpoint with same device code too fast, the server refuses to service your request. 
Use this information to decide how often to poll for ongoing authorization transaction status.

In case call fails, an error response is returned as described by "**Error Responses**" section, with following
possible `error` statuses: 

* **invalid_request**

Polling for Authorization Response (`/oauth/device/<ns>/<as-id>/token`)
------------------------------------------------------------------

After you have initiated an authorization transaction and told user to go to the `verification_uri` to enter the 
user code, you can start polling the gateways token endpoint for authorization result (which in successful case eventually results to access token response).

Following parameters are used with this operation:

Parameter        |  Description
:--------------- | :-----------------------
`grant_type`     | Tells to gateway that you're using the device code when requesting authorization tokens. With this flow, always use value *device_code*.
`device_code`    | The device code received in earlier step.

There is no need to include client credentials in this call.

Make following kind of HTTP POST request to the gateway:

    POST /oauth/device/d/mock/token HTTP/1.1
    Host: localhost
    Accept: application/json
    Content-Type: application/x-www-form-urlencoded

    grant_type=device_code&device_code=S2UJalLczDNixQrVsDmdbm4LEXTjYwc2


The response is result of the authorization process from the authorization server i.e. access token
response or an error response, if user declined authorization, or the authentication failed for any
reason. 

For instance after successfully completed authorization the response is **HTTP 200 OK** with JSON body:

    HTTP 200 OK
    Content-Type: application/json
    
	{
    	"access_token"  : "Lj1r3P2lsTBpQp95zHjVRmaYbdVI6c8pz3GG6oRNpX0FZRCt",
    	"token_type"    : "Bearer",
    	"refresh_token" : "OCeNFR81TQgqKmwohVWMmQR54jDWL1CDYaDTLHjQruwUWei2",
    	"expires_in"    : 3600, 
    	"issuer"        : "https://accounts.google.com"
	}

Note that the `refresh_token` and `expires_in` may also be absent, depending on the configuration 
and functionality of the authorization server. 

Also, if the AS supports [OpenID Connect](http://openid.net/connect/) and you included *openid* in the scope list, the response may also contain `id_token`. 

`issuer` attribute is set by the authorization gateway to indicate the actual authorization server that issued the tokens. If issuer is returned by the authorization server itself, the value is reflected back and the gateway does not override it. Issuer can be used by the API client to perform further check if it knows which authorization server should have been used. Otherwise this is mostly informative data.

If user denied authorization, or the authorization server declined the request, the response may be an **HTTP 400 Bad Request** response with JSON body. Like: 

    HTTP 400 Bad Request
    Content-Type: application/json
    
	{ 
		"error"             : "access_denied", 
		"error_description" : "The resource owner denied authorization."
	}

If authorization is still in progess i.e. not completed by the user, an error with status **authorization_pending** is returned instead:

	{ 
		"error" : "authorization_pending"
	}

In this case, wait at least the time defined by `interval` parameter in the device code response and then 
try again. 

If you make status requests faster than allowed by `interval`, you'll get a **slow_down** error response:

  	{ 
  		"error" : "slow_down"
  	}

If the transaction expires or the code has been used already (it can be used to request tokens only once), or some other grant related problem occurs that makes the used device code to be void, an error with status **invalid_grant** is returned instead:

    { 
       "error" : "invalid_grant", 
       "error_description" : "code used or expired"
    }

If you get some other error response than mentioned above, consult the "**Error Responses**" section for 
appropriate action. The list below shows all `error` statuses relevant for this API call:

* **authorization_pending**
* **slow_down**
* **invalid_grant**
* **access_denied**
* **unauthorized_client**
* **unsupported_response_type**
* **invalid_scope**
* **server_error**
* **temporarily_unavailable**

Refreshing Access Tokens (`/oauth/device/<ns>/<as-id>/token`)
---------------------------------------------------------

The gateway provides also way to refresh existing tokens from the authorizaton server. 
Perform a standard OAuth2 refresh token response. Client credentials are needed and 
recommended to pass them using **HTTP Basic**.

Following parameters are used with this request: 

Parameter        |  Description
:--------------- | :-----------------------
`grant_type`     | Tells to gateway that you're using the device code when requesting authorization tokens. With this flow, always use value *refresh_token*.
`refresh_token`  | The refresh token received in the previous access token response.
`scope`          | If you like to request new access token for another scope than the original authorization was granted to, tell the scopes with this parameter (space separated list). Depending on the authorization server, the scope value in refresh request may be supported or not.

Example request looks like: 

    POST /oauth/device/d/mock/token HTTP/1.1
    Host: localhost
    Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
    Accept: application/json
    Content-Type: application/x-www-form-urlencoded

    grant_type=refresh_token&refresh_token=OCeNFR81TQgqKmwohVWMmQR54jDWL1CDYaDTLHjQruwUWei2

Successful response to this request is again **HTTP 200 OK** with JSON containing new set of tokens:

    HTTP 200 OK
    Content-Type: application/json
    
	{
    	"access_token"  : "Lj1r3P2lsTBpQp95zHjVRmaYbdVI6c8pz3GG6oRNpX0FZRCt",
    	"token_type"    : "Bearer",
    	"refresh_token" : "OCeNFR81TQgqKmwohVWMmQR54jDWL1CDYaDTLHjQruwUWei2",
    	"expires_in"    : 3600,
    	"issuer"        : "https://accounts.google.com"
	}

Again, `expires_in` may or may not be present.

If request fails, an appropriate error response is returned. The error status depends on what the authorization server
returns but are generally the same as with the normal authorization token error response listed in previous section.

Error Responses
---------------

Normal error situations and failures in the API calls are returned with non-OK status codes: 

Status                      | Description
:-------------------------- | :-------------
400 Bad Request             | If one or more request parameters are malformed (missing or syntax error), or the authorization is still ongoing. 
401 Unauthorized            | If an access control related error occurs. Currently this is not used as the server API does not define its own access control.


The error response format is as described in the below example:

    HTTP 400 Bad Request
    Content-Type: application/json
    
	{
		"error": "invalid_request",
		"error_description": "unsupported response type"
	}

The `error` attribute defines the error status identifier. It is always present and should be used by the 
application client to implement error handling. The exact values depend on the use case. See the table below
for values used by the APIs.

Additionally, a textual `error_description` may be returned that gives further information about the possible cause of 
the problem. Applications must not depend on its presence or its content. It is either set by the gateway 
or it may be coming from the AS also, depending on situation. It is mostly useful for debugging and problem 
analysis.

Note that the same error response format is also used by the token polling API end point for some situations.

Possible values for **error** responses:

Error                       | Description
:-------------------------- | :-------------
`invalid_request`           |  If one or more request parameters are malformed (missing or syntax error).
`authorization_pending`     | The authorization process is still in progress and user has not completed it. This is normal situation until the process is completed or the transaction expires.
`slow_down`                 |  The application is polling status too fast and it must slow down. Always use the `interval` attribute from the device code request to check the shortest allowed time between two subsequent requests.
`invalid_grant`             | The device code is wrong, used grant type is not *device_code* (or *refresh_token*), or the authorization server rejected the request for some reason (e.g. invalid client credentials), or if the device code has expired  before the user completed authorization process.
`access_denied`             | The resource owner (user) or authorization server denied the request.
`unauthorized_client`       | The client is not authorized to request an authorization code using authorization code grant.
`unsupported_response_type` | The authorization server does not support obtaining an authorization code using  authorization code grant.
`invalid_scope`             | The requested scope is invalid, unknown, or malformed to the authorization server.
`server_error`              | The authorization server encountered an unexpected condition that prevented it from fulfilling the request, or the communication with the authorization server failed in a way considered permanent, or at least is expected to fail if tried again. The application should stop polling and cancel the current authorization transaction.
`temporarily_unavailable`   | The authorization server is currently unable to handle the request due to a temporary overloading or maintenance of the server. The application should stop polling with current pace, pause for some time and then again continue polling status for ongoing transaction.

You can test the API with any client credentials and without need to register anything anywhere
by using the [Mock Authorization Server](./MOCK_AS.md) bundled with the gateway.
