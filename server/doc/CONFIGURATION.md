OAuth2 Device Gateway Configuration
===================================

The server is configured with JSON configuration files located in *conf* directory.
Configuration is read using [nconf](https://github.com/flatiron/nconf) at startup time.

Settigs can also be passed in via environment settings or as arguments from command line, 
but normally they are defined in one or more JSON file.

Several configuration files may exist, they are all read in specific order and merged
together before exposing to application. This allows flexible ways to define default common values,  
and override them with local or deploy time settings.

Settings are read in following order: 

* Enviroment settings
* Startup arguments.
* Files `conf/config-X.json` in the order they are returned by file system.
* File `conf/local.json`
* File `conf/deploy.json`

The merged result is used as system configuration. 

Common Settings
----------------

Following configuration settings are used in the system in top level: 

* `interval`         : The default allowed polling interval by device applications in seconds.
* `validity`         : The time in seconds an authorization device code is valid until it expires.
* `audit_log_period` : How often to roll audit log files (e.g. 1d for one day). 
* `audit_log_count`  : How may audit log files to keep.
* `state_encryption_key` : Encryption key used to encrypt the *state* information passed in authorization requests. This can be overridden in authorization server settings for each server if needed.
* `state_encryption_alg` : Encryption algorightm used to encrypt the *state* information passed in authorization requests. This can be overridden in authorization server settings for each server if needed. Defaults to *aes256*.
* `client_credentials_send_method`: How to pass client credentials to authorization server in token requests. 
  This can be overridden in authorization server settings for each server if needed. Possible values can be **form**, defaults to HTTP Basic.
* `memcache`: Memcached server settings.  
* `idps` : List of identity providers / authorization servers that are loaded and allowed to be used. The value
is a map with key indicating the server key, and value as an attribute map with server specific configuration settings. 

See example structure below: 

    {
      "interval"                       : 1, 
      "validity"                       : 300,
      "audit_log_period"               : "1d",
      "audit_log_count"                : 3, 
      "state_encryption_key"           : "janisistuimaassa",
      "state_encryption_alg"           : "aes256",
      "client_credentials_send_method" : "form",
      "memcache" : ...,       
      "idps": ...
    }

Memcached Configuration
-----------------------

Memcache is used to store ongoing authorization transactions data and it is shared between the API and UI components.
Memcached server address is defined in the `memcache` attibute in configuration. It has following elements:

* `servers` : List of server addresses and ports that are used when storing temporary data. There can be one or more servers, each server address is defined as the key attribute of the list item and the value of the attribute if the relative weight of that server (see below for details).
* `options` : Additional configuration options e.g. to define consistent key hashing algorithms, timeouts etc.

Memcache connection is impemented with [memcached][memcached] module and configuration options defined in the configuration file are passed as is to the `Memcached` constructor. 

Example configuration below: 

    {
      "memcache"         : {
          "tx_cache_servers": {
              "192.168.60.11:11211" : 1,
              "192.168.60.12:11211" : 2
          },
          "credentials_cache_servers": {
              "192.168.60.13:11211" : 1,
              "192.168.60.14:11211" : 2
          },
          "options"      : {
            "maxExpiration" : 3600,
            "poolSize"      : 2,
            "algorithm"     : "md5",
            "reconnect"     : 60000,
            "timeout"       : 2500,
            "retries"       : 2,
            "failures"      : 5,
            "retry"         : 30000,
            "remove"        : false,
            "idle"          : 30000
          }
      }
    }

The transaction data cache can be differen than the cache used to store sensitive 
client credentials, if so desired. `credentials_cache_servers` setting is optional. If not set, the `tx_cache_servers` setting is used also for credentials cache.

For detailed information about the configuration settings see: 

* `XXX_servers` : [Memcached Server Locations][memcached-server-locations]
* `options` : [Memcached Options][memcached-options]

Identity Provider / Authorization Server Configuration
------------------------------------------------------

Several identity providers / authorization servers can be configured. All enabled servers are 
allowed to be used by any client (as the client credentials are validated by the target server, not the
gateway). Servers are configured under the `idps` top level element as in the example below: 

    {
      "idps": {
        "mock": {
             "name"      : "mock",
             "base_url"  : "http://localhost:3000",
             "authorize" : "/oauth/as/mock/authz",
             "token"     : "/oauth/as/mock/token"    
          }
      }
    }

Each server is identified by a key that is the name of the attribute in the `idps` dictionary. 
Server specific settings are defined as attribute map in the value of the server key: 

* `name`      : Short name of the server, used mostly for logging purposes.
* `base_url`  : The base URL where the server is reachable.
* `authorize` : Path relative to the base URL for the authorization endpoint.
* `token`     : Path relative to the base URL for the token endpoint.
* `state_encryption_key` : If different from common settings. 
* `state_encryption_alg` : If different from common settigs. 
* `client_credentials_send_method`: if different from common settings. 

Namespaces
----------

Namespaces are configuration elements that allow defininf environment / tenant or otherwise
system specific configuration settings and bind them to an authorization transaction via single
parameter. 

    {
      "namespaces": {
        "d": {
             "style"             : "default"
             "public_authority"  : "https://myshost.com"
          }
      }
    }

Namespace keys are used in URL paths and they are mapped in gateway server side to proper configuration.
It invalid namespace is used in URLs, requests will simply be rejected.

Namespaces are useful especially in multi-tenant setups where different UI styling and tenant specific
DNS names need to be used in user facing URLs for instance.

Logging Configuration
----------------------

Server logging uses [log4js](https://github.com/nomiddlename/log4js-node) for normal 
application logging and [bunyan](https://github.com/trentm/node-bunyan) for audit
logging. 

Log4js is configured in `conf/log4js.conf` and audit logging is 
setup directly in `lib/audit_log.js` module with only audit log file rolling
period and number of files to keep settings configurable in normal configuration.


[memcached]: https://github.com/3rd-Eden/node-memcached
[memcached-server-locations]: https://github.com/3rd-Eden/node-memcached/blob/master/README.md#server-locations
[memcached-options]: https://github.com/3rd-Eden/node-memcached/blob/master/README.md#options
