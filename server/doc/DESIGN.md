Device Authorization Gateway for OAuth2 
=======================================

The gateway connects to multiple OAuth2 compliant authorization servers (AS) using the 
*Authorization Code Grant* flow and takes care of user authentication and authorization 
process with the authorization server. The access and refresh tokens issued by the 
authorization servers are then given back to the original application. 

## System Architecture

The gateway works in two modes: 

   * REST API for device application to perform authorization requests and deliver 
     authorization responses from the authorization server to the device application.
   * Web UI that serves as the entry point for users to initialize the authorization 
     process with the selected authorization server.

Main system components are shown below:

![Main Components](./components.png)

The gateway supports only OAuth2 and utilizes *Authorization Code Grant* method with the authorization servers it interacts with.

### Transaction Cache

Currently memcache is used as temporary storage for ongoing transactions. Cache keys are encypted as well as all sensitive data. Client secret of the connecting clients are stored in (possibly) separate storage and are encypted. All data is deleted immediately when they are not needed anymore (e.g. cached access tokens are removed immediately after they have been delivered to connecting client.)


### CORS

CORS is enabled for all hosts in the API and user endpoints. This is in order to allow easy integration from browser only clients and there are no (known) security issues that might be caused by cross origin AJAX requests (as all client credentials must be registered to the actual authorization server in order for the gateway to function for those clients). 


Device Authorization API
========================

The main API is HTTP based and it is described in [API](./DEVICE_AUTH_API.md) 
documentation.

API can be used directly or via a separate client SDK library, which is 
described in [Client SDK](../../sdk/README.md) documentation.

OAuth2 Device Gateway Configuration
===================================

The server is configured with JSON configuration files located in *conf* directory.
Configuration is read using [nconf](https://github.com/flatiron/nconf) at startup time.

See [CONFIGURATION](./CONFIGURATION.md) for details about the configuration model.

(Audit) Logging
===============

System generates  detailed audit events from every authorization transaction for tracking the 
usage later if needed. Audit logging is further explained in [LOGGING](./LOGGING.md).