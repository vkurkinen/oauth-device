(Audit) Logging
===============

Log files are generated to `log` directory under the main application directory.
Audit events are created from every authorization transaction and they contain following information:

* Datetime of the event.
* Transaction ID, same for every event related to same authorization transaction.
* The OAuth2 client identifier that requested authorization
* Status of each operation (failed, success)
* Type of each event.
* Identity provider / authorization server identifier.
* Device, user and authorization codes involved in the process. 

The types of events tracked are: 

* A new authorization transaction is requested by a client.
* End user confirms the user code and initiates authorization code grant flow.
* Result of the authorization process.
* The device application receiving the result of the authorization process. 
* Device poll with a device code that is not valid anymore (to detect misuse).
* Refreshing of existing tokens from the authorization server with a refresh token (this has own tx_id as it is not tracked back to original authorization.)

Audit log files are best read with `bunyan` binary. E.g:

    $ less log/audit.log | node_modules/bunyan/bin/bunyan -c 'this.tx_id == "910b84ed-5665-474f-8b3a-34faf5b82baf"'
    [2014-03-23T20:02:18.478Z]  INFO: audit/26419 on einstein: new device auth entry request from client (type=device.auth, idp=g, tx_id=910b84ed-5665-474f-8b3a-34faf5b82baf)
        client_id: NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com
        --
        ctx: {
          "idp": "g",
          "device_code": "Dqf1xZudXc6qzmX7F3yxTetetdCWXMAs7ls",
          "client_id": "NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com",
          "action": "create",
          "status": "success"
        }
    [2014-03-23T20:02:33.963Z]  INFO: audit/26419 on einstein: code confirmed by user and auth in progress (type=device.auth, idp=g, tx_id=910b84ed-5665-474f-8b3a-34faf5b82baf)
        client_id: NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com
        --
        ctx: {
          "idp": "g",
          "device_code": "Dqf1xZudXc6qzmX7F3yxTetetdCWXMAs7ls",
          "user_code": "397679",
          "client_id": "NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com",
          "action": "in_progress",
          "status": "success"
        }
    [2014-03-23T20:02:34.699Z]  INFO: audit/26419 on einstein: device auth completed by user (type=device.auth, idp=g, tx_id=910b84ed-5665-474f-8b3a-34faf5b82baf)
        client_id: NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com
        --
        ctx: {
          "idp": "g",
          "device_code": "Dqf1xZudXc6qzmX7F3yxTetetdCWXMAs7ls",
          "client_id": "NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com",
          "action": "completed",
          "status": "success",
          "authz_code": "4/wFwehwre06Klwow9GOpwet4E_QmsTQ.kpKtWK85v6EZPvB8fYmgkJzp4hbhiQI",
          "result": "tokens"
        }
    [2014-03-23T20:02:37.904Z]  INFO: audit/26419 on einstein: device auth entry used by client (type=device.auth, idp=g, tx_id=910b84ed-5665-474f-8b3a-34faf5b82baf)
        client_id: NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com
        --
        ctx: {
          "idp": "g",
          "device_code": "Dqf1xZudXc6qzmX7F3yxTetetdCWXMAs7ls",
          "client_id": "NNNNNckeak1rjiejmknhji0.apps.googleusercontent.com",
          "action": "used",
          "status": "success",
          "result" : "tokens"
        }



