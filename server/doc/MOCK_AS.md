Using Built-in Mock Authorization Server
========================================

For quick testing and prototyping the gateway includes a built-in mock authorization server. 
It provides no UI but automatically issues tokens for any client credentials and 
allows querying basic user profile information with the access token issued
by the Mock AS.

How to Tell Gateway to Use Mock AS
------------------------------

The mock as is used when the `<as-id>` parameter in the gateway URL is set to value **mock**.

URL Endpoints
--------------

The Mock AS provides following URLs to be used: 

    http://<host>/oauth/as/d/mock/authz
    http://<host>/oauth/as/d/mock/token
    http://<host>/oauth/as/d/mock/profile

**authz** endpoint is used to for performing the Authorization Code Grant flow.

**token** endpoint is used for exchaning the authorization code to access token (and for refreshing tokens) as per standard OAuth2 mechanism

**profile** endpoint is used to query user profile with access token.

Querying User Profile With Mock AS
------------------------------

Mock AS implements a simple UserInfo endpoint that is compliant with the [OpenID Connect 1.0 UserInfo][openid-connect-1.0-user-info] endpoint for querying basic user profile data representing the resource owner. The data returned is static but the `profile` and `email` scopes are honored (that is, *email* address attribute is not returned if `email` scope is not requested).

When querying user attributes, perform following HTTP GET: 

    GET /oauth/as/d/mock/profile HTTP/1.1
    Host: localhost
    Authorization : Bearer <access-token>
    Accept: applicaton/json
   
Do not include client credentials in this request.   
         
The response in success case looks like:

    {
        "given_name"         : "Mock",
        "family_name"        : "User",
        "preferred_username" : "mock.user",
        "email"              : "mock.user@mocked.nodomain",
        "name"               : "Mock User",
        "sub"                : "c70298fd-6c4d-4ef5-8069-e165879a6d5a"
    }
    
**NOTE**: Responses returned by real user profile servers are usually different. 

You can also send the initial client credentials as **HTTP Basic** authentication in the *Authorization* header but in this case include access token in **query** parameter `access_token`. This mechanism is mostly intended to be used as alternative as it is required by some identity providers.

The alternative way would thus be like so:

    GET /oauth/as/d/mock/profile?access_token=<access-token> HTTP/1.1
    Host: localhost
    Authorization : Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
    Accept: applicaton/json 


[openid-connect-1.0-user-info]: http://openid.net/specs/openid-connect-basic-1_0.html#UserInfo