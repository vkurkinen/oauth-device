/**
* Implements the web layer of simple Mock OAuth2 AS to be used for offline
* development locally.
*
* Only Authorization Code Grant is supported.
*/
var codegen = require('./codegen');
var log     = require('log4js').getLogger("mock");
var _ 		= require('underscore');


function OAuth2MockAS(config, audit_log) {
	this.config          = config;
	this.audit           = audit_log;
	this.entry_storage = new MockAuthzEntryStorage(config.get('memcache'));
};

/**
Process authorization request.
Currently only Authorization Code Grant flow supported.
*/
OAuth2MockAS.prototype.authz = function(client_id, scope) {
    var now = new Date().getTime();
	var entry = {};
	// now only authz code flow...
	entry.authz_code       = codegen.code();
	entry.created_time     = now;
	entry.expiry_time      = now + (3600 * 1000);
	entry.used_time        = null;
	entry.client_id        = client_id;
	entry.scope            = (scope) ? scope.split(' ') : [];
	entry.id               = codegen.txid('M:');

	var ttl = entry.expiry_time - entry.created_time;
	this.entry_storage.put(entry);
	log.debug("new authz code entry: " + JSON.stringify(entry));
	return entry;
};

OAuth2MockAS.prototype.code_token = function(authz_code, client_id, client_secret, cb) {

	log.info("recvd code -> token: authz_code " + authz_code);

	var now = new Date().getTime();
	var entry_storage = this.entry_storage;
	entry_storage.getByAuthzCode(authz_code, function (entry) {
		if (entry == null) {
			cb("not_found", null);
			return;
		}
		if (!check_client_credentials(entry, client_id, client_secret,cb)) {
			return;
		}
		if (entry.expiry_time < now || entry.used_time != null) {
			log.warn("entry has expired or used for code " + authz_code);
			cb("not_found", null);
			return;
		}
		entry.tokens = new_token();
		entry.used_time = now;
		// store entry also with tokens as keys for later access
		// in refresh and validate phases.
		var ttl = entry.tokens.expires_in * 1000;
		entry.expiry_time = (now + ttl) + (ttl * 10);
		entry_storage.put(entry);
		cb(null, entry.tokens);
	});
};

OAuth2MockAS.prototype.refresh_token = function(refresh_token, scope, client_id, client_secret, cb) {
	var now = new Date().getTime();
	var entry_storage = this.entry_storage;
	entry_storage.getByRefreshToken(refresh_token, function (entry) {
		if (entry == null || entry.tokens == null) {
			cb("invalid_grant", null);
			return;
		}
		if (!check_client_credentials(entry, client_id, client_secret,cb)) {
			return;
		}
		if (entry.expiry_time < now) {
			log.warn("entry has expired for refresh token " + refresh_token);
			entry_storage.remove(entry);
			cb("invalid_grant", null);
			return;
		}
		if (scope && scope != entry.scope) {
			log.warn("only original scope can be requested in this mock, not " + scope);
			cb("invalid_scope", null);
			return;
		}
		// else, all is good, assign new refresh token
		entry_storage.remove(entry, {});

		entry.tokens = new_token();
		entry.used_time = now;
		var ttl = entry.tokens.expires_in * 1000;
		entry.expiry_time = (now + ttl) + (ttl * 10);
		entry_storage.put(entry);
		cb(null, entry.tokens);
	});
};

OAuth2MockAS.prototype.openid_connect_userinfo = function(access_token, client_id, client_secret, cb) {
	var now = new Date().getTime();
	this.entry_storage.getByAccessToken(access_token, function (entry) {
		if (entry == null || entry.tokens == null) {
			log.info("no entry found for requested access token, rejecting validation request");
			cb("invalid_grant", null);
			return;
		}
        if (!check_client_credentials(entry, client_id, client_secret,cb)) {
			return;
		}
        var expires_in = Math.floor((entry.expiry_time - now) / 1000);
		if (expires_in <= 0) {
			cb("invalid_grant", null);
			return;
		}

	    var token = entry.tokens;
	    var attrs = entry.attrs;
	    if (!attrs) {
	    	// quite static data for now...
			attrs = {};
            attrs.sub = codegen.uuid();
            if (_.contains(entry.scope,'profile')) {
				attrs.given_name  = "Mock";
				attrs.family_name = "User";
				attrs.preferred_username = "mock.user";
				attrs.name = "Mock User";
			};
			entry.attrs = attrs;
        }
        if (_.contains(entry.scope,'email')) {
            attrs.email = "mock.user@nodomain.com";
        }

	    if (true) {
			cb(null, attrs);
		} else {
		   cb("invalid_grant", null);
		}
	});
};

function new_token() {
	var token = {
		access_token  : codegen.token(),
		refresh_token : codegen.token(),
		expires_in    : 300,
		token_type    : 'Bearer'
	};
	return token;

}

function check_client_credentials(entry, client_id, client_secret, cb) {
    // client credentials are optional in mock but if present they must match.
    if (!client_id && !client_secret) {
        return true;
    }

	if (entry.client_id != client_id) {
		log.warn("invalid client_id in request");
		cb("invalid_client", null);
		return false;
	}

	if (!client_secret) {
		log.warn("missing client_secret in request");
		cb("invalid_client", null);
		return false;
	}
	return true;
}

// memcached backed storage for authz entries
function MockAuthzEntryStorage(config) {
	var Memcached  = require('memcached');
	this.cache     = new Memcached(config.tx_cache_servers, config.options);
	log.info("using memcache with servers " + JSON.stringify(config.tx_cache_servers));
};

MockAuthzEntryStorage.prototype.put = function(entry) {
	var now = new Date().getTime();
  	var ttl = Math.floor((entry.expiry_time - now) / 1000);
  	if (entry.authz_code != null) {
  		// short expiry time for authz code.
    	this.putEntry("as://mock/authz/" + entry.authz_code, entry, Math.floor(ttl / 10));
    }
  	if (entry.tokens != null && entry.tokens.access_token != null) {
  		this.putEntry("as://mock/access/" + entry.tokens.access_token, entry, ttl);
  	}
	if (entry.tokens != null && entry.tokens.refresh_token != null) {
		// longer TTL to enable refresh token usage better.
		this.putEntry("as://mock/refresh/" + entry.tokens.refresh_token, entry, ttl * 10);
  	}
};

MockAuthzEntryStorage.prototype.remove = function(entry) {
  	this.deleteEntry("as://mock/authz/" + entry.authz_code);
	if (entry.tokens && entry.tokens.access_token) {
	  	this.deleteEntry("as://mock/access/" + entry.tokens.access_token);
	}
	if (entry.tokens && entry.tokens.refresh_token) {
	  	this.deleteEntry("as://mock/refresh/" + entry.tokens.refresh_token);
	}
}

MockAuthzEntryStorage.prototype.getByAuthzCode = function(code, callback) {
  	this.getEntry("as://mock/authz/" + code, callback);
}

MockAuthzEntryStorage.prototype.getByAccessToken = function(token, callback) {
  	this.getEntry("as://mock/access/" + token, callback);
}

MockAuthzEntryStorage.prototype.getByRefreshToken = function(token, callback) {
  	this.getEntry("as://mock/refresh/" + token, callback);
}

MockAuthzEntryStorage.prototype.putEntry = function (key, entry, ttl) {
	var data = JSON.stringify(entry);
  	this.cache.set(key, data, ttl, function (error, result) {
  		if (error) {
  			log.warn("failed to add entry to cache: " + error + " (" + result + ")");
  		}
  	}, ttl);
}

MockAuthzEntryStorage.prototype.deleteEntry = function (key) {
  	this.cache.del(key, function (error, result) {
	  	if (error) {
	  		log.warn("failed to delete entry from cache: " + error + " (" + result + ")");
	  	}
	});
}

MockAuthzEntryStorage.prototype.getEntry = function (key, callback) {
   	this.cache.get(key, function (error, result) {
  		if (error) {
  			log.warn("failed to get entry from cache: " + error + " (" + result + ")");
  		}
  		if (result) {
  			callback(JSON.parse(result));
  		} else {
  			callback(null);
  		}
  	});
}
module.exports = OAuth2MockAS;