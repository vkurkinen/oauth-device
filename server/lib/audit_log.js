var bunyan  = require('bunyan');

// setup audit log

function AuditLog(file, period, count) {
	this.main_log = bunyan.createLogger(
		{ name: "audit", 
			streams: [{
				type   : 'rotating-file',
				path   : file,
				period : period, 
				count  : count 
			}]			
		});
	this.auth_log = this.main_log.child({type: 'device.auth'});	
	this.new_auth_log = this.auth_log.child({type: 'new'});	
}


AuditLog.prototype.new_auth = function (entry, status, error) {
	var event = {
		tx_id       : entry.id,
		idp         : entry.idp_key,
		ns          : entry.ns_key,
		lang        : entry.locale,
		device_code : entry.device_code,
		client_id   : entry.client_id,
		action      : 'create',
		status      : status
	};
	add_error_to_event(event, error);
	this.auth_log.info(build_context(event), 'new device auth entry request from client');
};

AuditLog.prototype.device_poll = function (entry, status, error) {
	var event = {
		tx_id       : entry.id,
		idp         : entry.idp_key,
		ns          : entry.ns_key,
		lang        : entry.locale,
		device_code : entry.device_code,
		client_id   : entry.client_id,
		action      : 'access',
 		status      : status
 	};
	add_error_to_event(event, error);
 	this.auth_log.info(build_context(event), 'device auth entry accessed by client');		
};

AuditLog.prototype.device_use = function (entry, status, error) {
	var event = {
		tx_id       : entry.id,
		idp         : entry.idp_key,
		ns          : entry.ns_key,
		lang        : entry.locale,
		device_code : entry.device_code,	
		client_id   : entry.client_id,
		action      : 'used',
 		status      : status
 	}; 	
 	if (entry.tokens) {
 		// do not log tokens
 		//event.result = entry.tokens;
 		event.result = 'tokens'; 		
 	} 
 	if (entry.error) {
 		event.result = entry.error;
 	}

	add_error_to_event(event, error);
 	this.auth_log.info(build_context(event), 'device auth entry used by client');		
};

AuditLog.prototype.auth_in_progress = function (entry, status, error) {
	var event = {
		tx_id       : entry.id,
		idp         : entry.idp_key,
		ns          : entry.ns_key,
		lang        : entry.locale,
		device_code : entry.device_code,
		user_code   : entry.user_code,
		client_id   : entry.client_id,
		action      : 'in_progress',
 		status      : status
 	};
	add_error_to_event(event, error);
 	this.auth_log.info(build_context(event), 'code confirmed by user and auth in progress');		
};


AuditLog.prototype.auth_completed = function (entry, status, error) {
	var event = {
		tx_id       : entry.id,
		idp         : entry.idp_key,
		ns          : entry.ns_key,
		lang        : entry.locale,
		device_code : entry.device_code,
		client_id   : entry.client_id,
		action      : 'completed',
 		status      : status
 	};
 	if (entry.authz_code) {
 		event.authz_code = entry.authz_code;
 	}
 	if (entry.tokens) {
 		// do not log tokens
 		//event.result = entry.tokens;
 		event.result = 'tokens'; 		
 	} 
 	if (entry.error) {
 		event.result = entry.error;
 	}
	add_error_to_event(event, error);
 	this.auth_log.info(build_context(event), 'device auth completed by user');		
};

// TODO: add audit event from refresh

function add_error_to_event(event, error) {
	if (error) {
		event.error = error.error;
		if (error.error_description) {
			event.error_description = error.error_description;
		}
	}
}

function build_context(event) {
	var data = {
		idp       : event.idp,
		ns        : event.ns,
		lang      : event.locale,
		client_id : event.client_id,
		ctx       : event, 
		tx_id     : event.tx_id		
	};
	data.ctx.tx_id = undefined;
	return data;
}

module.exports = AuditLog;
