
/**
* Some common utilities.
*/

exports.error = function(res, error, error_description) {	
	var data = {
		error : error
	}
	if (error_description != null) {
		data.error_description = error_description;
	}
	res.json(400, data);	
};

exports.response = function(res, data) {
	if (data.error) {
	   res.json(400, data);
	} else {
	   res.json(data);
	} 
};

