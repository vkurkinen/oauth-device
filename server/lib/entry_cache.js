/**
* Implements the entry cache for ongoing device authorization entries.
* Entries are stored into remote memcached instance.
*/
var Memcached = require('memcached');

var DeviceAuthEntry = require('./entry');
var CredentialsEntry = require('./client_credentials_entry');

var codegen= require('./codegen');
var log    = require("log4js").getLogger("cache");

/**
* Create new entry cache with the given memcached configuration (host, port).
*/
function OAuth2DeviceEntryCache(config, crypter) {
  log.info("configuring memcached with tx servers: " + JSON.stringify(config.tx_cache_servers));
  log.info("configuring memcached with cred servers: " + JSON.stringify(config.credentials_cache_servers));

  log.debug("memcached configuration options: " + JSON.stringify(config.options));

  this.crypter = crypter;

	this.cache = new Memcached(config.tx_cache_servers, config.options);

  var credentials_cache_servers = config.credentials_cache_servers || config.tx_cache_servers;
  this.credentials_cache = new Memcached(credentials_cache_servers, config.options);
  this.keyBuilder = new CacheKeyBuilder(crypter);
};

/**
* Put a new entry to the cache.
*/
OAuth2DeviceEntryCache.prototype.put = function (entry) {
    var ttl = Math.ceil((entry.expiry_time - entry.created_time)/ 1000);

    // store both device code and user code as keys.
    var device_key      = this.keyBuilder.key_device_code(entry.idp_key, entry.device_code);
    var user_key        = this.keyBuilder.key_user_code(entry.idp_key, entry.user_code);

    var credentials_entry           = new CredentialsEntry();
    credentials_entry.client_secret = entry.client_secret;
    credentials_entry.idp_key       = entry.idp_key;
    var credentials = credentials_entry.encrypt(this.crypter);

    // do not store client secret to normal entry data, but only the ref key used
    // to look it up separately.
    entry.client_secret = null;
    entry.client_secret_ref = codegen.token();

    var data       = JSON.stringify(entry);
    this.cache.set(device_key, data, ttl, callback_set);
    this.cache.set(user_key, data, ttl, callback_set);

    var credentials_key = this.keyBuilder.key_credentials(entry.idp_key, entry.client_secret_ref);
    this.credentials_cache.set(credentials_key, credentials, ttl, callback_set);
};

/**
* Update an entry to the cache.
*/
OAuth2DeviceEntryCache.prototype.update = function (entry) {
  	var ttl = Math.ceil((entry.expiry_time - entry.created_time)/ 1000);
    // store both device code and user code as keys.
    var device_key      = this.keyBuilder.key_device_code(entry.idp_key, entry.device_code);
    var user_key        = this.keyBuilder.key_user_code(entry.idp_key, entry.user_code);

    // encrypt token response, if those exist.
    if (entry.tokens != null) {
      entry.tokens = this.crypter.encrypt(entry.idp_key, entry.tokens);
    }

    // clear client secret, in case it is set before updating data to cache
    entry.client_secret = null;
    var data = JSON.stringify(entry);
    this.cache.set(device_key, data, ttl, callback_set);
    this.cache.set(user_key, data, ttl, callback_set);
};

/**
* Get entry from cache based on the device code. Notifies callback with the entry
* when it is available, or with "null" if it is not available (for any reason).
*/
OAuth2DeviceEntryCache.prototype.getByDeviceCode = function (idp_key, device_code,callback) {
  var self = this;
  var device_key = this.keyBuilder.key_device_code(idp_key, device_code);
	this.cache.get(device_key, function (error, result) {
		if (error) {
			log.warn("failed to get entry from cache: " + error);
		}
    self.notify(result, callback);
	});
};

/**
* Get entry from cache based on user code. Notifies callback when entry is available
* or operation has otherwise completed.
*/
OAuth2DeviceEntryCache.prototype.getByUserCode = function (idp_key, user_code, include_credentials, callback) {
    var self = this;
    var user_key = this.keyBuilder.key_user_code(idp_key, user_code);

  	this.cache.get(user_key, function (error, result) {

      if (error) {
        log.warn("failed to get entry from cache: " + error + " (" + result + ")");
      }

      self.notify(result, function(entry) {
        if (entry == null || !include_credentials) {
          callback(entry);
          return;
        }

        var credentials_key = self.keyBuilder.key_credentials(idp_key, entry.client_secret_ref);
        self.credentials_cache.get(credentials_key, function (error, credentials) {
          if (error) {
            log.warn("failed to get entry from credentials cache: " + error + " (" + result + ")");
            callback(entry);
            return;
          }
          if (credentials && credentials != null) {
            var ce = new CredentialsEntry();
            ce.idp_key = entry.idp_key;
            ce.decrypt(credentials, self.crypter);
            entry.client_secret = ce.client_secret;
          }
          callback(entry);
        });
      });
    });
};

/**
* Remove given entry from cache.
*/
OAuth2DeviceEntryCache.prototype.remove = function (entry) {
    var device_key      = this.keyBuilder.key_device_code(entry.idp_key, entry.device_code);
    var user_key        = this.keyBuilder.key_user_code(entry.idp_key, entry.user_code);
    var credentials_key = this.keyBuilder.key_credentials(entry.idp_key, entry.client_secret_ref);

    this.credentials_cache.del(credentials_key, callback_delete);
  	this.cache.del(device_key, callback_delete);
  	this.cache.del(user_key, callback_delete);
};

OAuth2DeviceEntryCache.prototype.notify = function (entry_data, callback) {
  var entry = null;
  if (entry_data) {
    try {
      entry = new DeviceAuthEntry(JSON.parse(entry_data));
      if (entry.tokens != null) {
        entry.tokens = this.crypter.decrypt(entry.idp_key, entry.tokens);
      }
    } catch (e) {
      log.error("failed to parse data from cache! data -> " + result + ": ERR -> " + e);
    }
  }
  callback(entry);
}

function callback_set(error, result) {
  if (error) {
    log.warn("failed to set entry to cache: " + error + " (" + result + ")");
  }
};

function callback_delete(error, result) {
  if (error) {
    log.warn("failed to delete entry from cache: " + error + " (" + result + ")");
  }
};

// generates keys for cache entries

function CacheKeyBuilder(crypter) {
  this.crypter = crypter;
}

// build cache key for the device code
CacheKeyBuilder.prototype.key_device_code = function (idp_key, device_code) {
  return "devauth://" + idp_key + "/device/" + this.crypter.encrypt(idp_key,device_code);
}

// build cache key for the user code
CacheKeyBuilder.prototype.key_user_code = function (idp_key, user_code) {
  return "devauth://" + idp_key + "/user/" + this.crypter.encrypt(idp_key,user_code);
}

// build cache key for client credentials entry
CacheKeyBuilder.prototype.key_credentials = function (idp_key, ref) {
  return "devauth://" + idp_key + "/key/" + this.crypter.encrypt(idp_key,ref);
}

module.exports = OAuth2DeviceEntryCache;
