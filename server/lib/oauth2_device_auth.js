/**
* The main Device OAuth2 Gateway service implementation.
* Manages creation and acces of device auth entries and
* communication with the remote IdPs,
*/

var _ = require('underscore');

var codegen                = require('./codegen');
var OAuth2DeviceEntryCache = require('./entry_cache');
var DeviceAuthEntry        = require('./entry');
var IdPRegistry            = require('./idp_registry');
var NamespaceRegistry      = require('./ns_reg');
var Crypter     		   = require('./crypter');

var log                    = require('log4js').getLogger("auth");
var request     		   = require('request');
var merge 			       = require('merge');

var metrics				   = require('./metrics');
/**
* Create new service instance.
*/
function OAuth2DeviceAuth(config, audit_log) {
	this.common_request_options = { strictSSL: false };

	this.audit         = audit_log;
	this.config        = config;
	this.idp_registry  = new IdPRegistry(config);
	this.ns_registry   = new NamespaceRegistry(config);

	this.idp_registry.init();
	this.ns_registry.init();

	this.crypter = new Crypter(this.idp_registry);

	this.entry_storage = new OAuth2DeviceEntryCache(config.get('memcache'), this.crypter);

	this.stats = metrics.newGlobalStats();
};

/**
* Get an IdP entry from the internal registry.
*/
OAuth2DeviceAuth.prototype.getIdP = function(key) {
	return this.idp_registry.find(key);
};

/**
* Register a new IdP to the registry of this service. This is mainly used
* from tests to inject a test IdP during the tests.
*/
OAuth2DeviceAuth.prototype.registerIdP = function(idp_key, idp) {
	return this.idp_registry.register_idp(idp_key, idp);
};

/**
* Get a namespace entry from the internal registry.
*/
OAuth2DeviceAuth.prototype.getNamespace = function(key) {
	return this.ns_registry.find(key);
};

/**
* Register a new namespace to the registry of this service. This is mainly used
* from tests to inject a test namespace during the tests.
*/
OAuth2DeviceAuth.prototype.registerNamespace = function(ns_key, ns) {
	return this.ns_registry.register_namespace(ns_key, ns);
};

/**
* Create a new device authorization entry for the given client and scope in the specified IdP.
*/
OAuth2DeviceAuth.prototype.newDeviceEntry = function(idp, ns, locale, client_id, client_secret, scope) {
	var now = new Date().getTime();
	var entry = new DeviceAuthEntry();
	entry.device_code      = codegen.code();
	entry.user_code        = codegen.pin();
	entry.interval         = this.config.get('interval');
	entry.created_time     = now;
	entry.expiry_time      = now + (this.config.get('validity') * 1000);
	entry.last_access_time = 0; // let one poll be done before triggering flow control.
	entry.completed_time   = null;
	entry.used_time        = null;
	entry.tokens           = null;
	entry.error            = null;
	entry.idp_key          = idp.key;
	entry.ns_key           = ns.key;
	entry.issuer           = idp.issuer();
	entry.locale           = locale;
	entry.client_id        = client_id;
	entry.client_secret    = client_secret;
	entry.scope            = scope;
	entry.id               = codegen.txid('0:');

	// NOTE: client secret gets encrypted after put to cache and is only available when separately requested.
	this.entry_storage.put(entry);
	log.debug("issued new device authz for client " + entry.client_id + " in idp " +
		       entry.idp_key + " with code: " + entry.device_code + ", tx_id: " + entry.id);

	this.audit.new_auth(entry, 'success');

	idp.stats.deviceCodeRequested.mark();
	this.stats.deviceCodeRequested.mark();

	return entry;
};

/**
* Handle request from remote device to poll for tokens.
* This checks the internal status of the device entry and if it has been
* completed by user (either successfully or failed), return the result.
*
* It entry is still in progress, and not expired, return "authorization pending" error.
* If requests are coming too fast, return "slow down" error.
*
* All results are sent to the provided callback function.
*/
OAuth2DeviceAuth.prototype.processDevicePollRequest = function(idp, device_code, cb) {
	var self = this;
	var storage = this.entry_storage;
	var audit   = this.audit;

	idp.stats.codePoll.mark();
	self.stats.codePoll.mark();

	storage.getByDeviceCode(idp.key, device_code,function (entry) {
		log.debug("device poll with code " + device_code + ": " + ((entry) ? entry.id : '<unknown>'));
		var error = null;
		if (entry == null) {
			error =  {
				error: 'invalid_grant',
			  	error_description : 'unknown device code'
		    }
		    // no audit logging for unknown entries

		    idp.stats.codePollUnkownCode.mark();
			self.stats.codePollUnknownCode.mark();

		} else if (entry.isExpired()) {
			log.warn("entry expired for code " + device_code + ", removing it");
			storage.remove(entry);
			error = {
			   error : 'invalid_grant',
			   error_description : 'code expired'
			};
			audit.device_poll(entry, 'failed', error);
			entry = null;

			idp.stats.codePollExpiredCode.mark();
			self.stats.codePollExpiredCode.mark();

		} else if (entry.isUsed()) {
			log.warn("entry used for code " + device_code + ", rejecting request");
			error = {
			  	error : 'invalid_grant',
			    error_description : 'code already used'
			};
			audit.device_poll(entry, 'failed', error);

			idp.stats.codePollUsedCode.mark();
			self.stats.codePollUsedCode.mark();

		} else if (entry.isInProgress()) {
			if (entry.shouldSlowDown()) { // flow control
				log.warn("device poll too fast from client " + entry.client_id + ", requesting slow down!");
			    error =  { error : 'slow_down'};

				idp.stats.codePollFlowControl.mark();
				self.stats.codePollFlowControl.mark();

			} else { // user is still doing authorization.
			   log.debug("device authz in progress from client " + entry.client_id + " for code " + entry.device_code);
			   error = { error: 'authorization_pending'};
			   entry.touch();
			   storage.update(entry);
			}
 	    }
		if (error) {
			cb(error, null);
			return;
		}

		// else the entry has completed and is OK to access it
		cb(null, entry);

	    // mark it as "used" so it cannot requested again. This is one time thing.
		entry.used();

		// remove tokens so they cannot be accessed anymore.
		entry.tokens = null;

		storage.update(entry);
		audit.device_use(entry, 'success');

		var roundtripTime = Date.now() - entry.created_time;
		idp.stats.deviceCodeCompleted.update(roundtripTime);
		self.stats.deviceCodeCompleted.update(roundtripTime);

		log.debug("device authz done from client " + entry.client_id + " for code " + entry.device_code);
	});
};

/**
* Handle user code request i.e. the case when user enters the code in the UI and submits form.
* This looks up the entry from the cache and if found and is still valid, returns it to the callback.
*/
OAuth2DeviceAuth.prototype.processUserCodeRequest = function(idp, user_code, cb) {
	if (!user_code) {
		cb('invalid_user_code', null);
		return;
	}
	var self = this;

	idp.stats.userCodeSubmit.mark();
	self.stats.userCodeSubmit.mark();

	this.entry_storage.getByUserCode(idp.key, user_code, false, function (entry) {
		log.debug("process user code request (" + user_code + ") -> " + JSON.stringify(entry));
		var error = null;
		// check that the entry is still valid and usable.
		if (!entry) {
			error = 'invalid_user_code';
			idp.stats.userCodeUnknown.mark();
			self.stats.userCodeUnknown.mark();
		} else if (entry.isExpired()) {
			error = 'invalid_grant';
			idp.stats.userCodeError.mark();
			self.stats.userCodeError.mark();
		} else if (entry.isCompleted()) {
			error = 'invalid_grant';
			idp.stats.userCodeError.mark();
			self.stats.userCodeError.mark();
		} else if (entry.isUsed()) {
			error = 'invalid_grant';
			idp.stats.userCodeError.mark();
			self.stats.userCodeError.mark();
		}

		if (error != null) {
			cb(error, null, null);
			return;
		}

		idp.stats.authorizationStart.mark();
		self.stats.authorizationStart.mark();

		var state = {user_code : user_code};
        cb(null, entry, idp.encode_data(state));
	});
};

/**
* Get raw entry by device code if it exists. This is only used for internal testing.
*/
OAuth2DeviceAuth.prototype.getEntryByDeviceCode = function(idp, device_code, cb) {
	this.entry_storage.getByDeviceCode(idp.key, device_code,function (entry) {
		var error = null;
		if (!entry) {
			cb('invalid_grant', null);
		} else {
    		cb(null, entry);
    	}
	});
};

/**
* Handle oauth authz code grant callback response from the IdP.
*/
OAuth2DeviceAuth.prototype.processAuthzCallbackRequest = function(idp, authz_data, req, cb) {


	var state = null;
	try {
		state = idp.decode_data(authz_data.state);
	} catch (err) {
		log.error("failed to decode state: " + err);
	}

	if (state == null || !state.user_code) {
		cb('invalid_request', null);
		return;
	}

	var self = this;

	this.entry_storage.getByUserCode(idp.key, state.user_code, true, function (entry) {
		var error = null;
		if (!entry) {
			error = 'invalid_grant';
			idp.stats.authorizationExpired.mark();
			self.stats.authorizationExpired.mark();
		} else if (entry.isExpired()) {
			log.info("request for an expired entry, handling as expired");
			error = 'invalid_grant';
			idp.stats.authorizationExpired.mark();
			self.stats.authorizationExpired.mark();
		}
		if (error) {
    		cb(error, null);
    		return;
    	}

		// allow replay in case case user refreshes the page...
		if (entry.isDone() || entry.isUsed()) {
			if (authz_data.code == entry.authz_code) {
				log.info("replay detected with valid authz_code, returning existing result");
				if (entry.getError()) {
					cb(entry.getError().error, entry);
				} else {
					cb(null, entry);
				}
			} else {
				log.warn("replay attempt detected with wrong authz_code, rendering UI directly");
				cb('invalid_request', null);
			}
			return;
		}

		// otherwise, proceed with authz code result

		// check if user denied or other failure
		if (authz_data.error) {
			var error = {
				error : authz_data.error
			};
			if (authz_data.error_description) {
				error.error_description = authz_data.error_description;
			}
			log.info("recvd error callback from AS flow: " + JSON.stringify(error));
			entry.error = error;

			if (authz_data.error === 'access_denied') {
				idp.stats.authorizationAccessDenied.mark();
				self.stats.authorizationAccessDenied.mark();
			} else {
		  		idp.stats.authorizationOtherError.mark();
				self.stats.authorizationOtherError.mark();
		  	}

			self.authzCompleted(entry);
			cb(entry.error.error, entry);
			return;
		}

		// else, authorization succeeded so fetch tokens.
		var options = { followRedirect: false };
		options = merge(options,self.common_request_options);

		var body = {
				grant_type   : 'authorization_code',
				code	     : authz_data.code,
				redirect_uri : idp.callback_uri(req)
			};
		if (idp.client_credentials_send_method == 'form') {
			body.client_id 	   = entry.client_id;
			body.client_secret = entry.client_secret;
		} else {
			options.auth = {
				 user: entry.client_id,
				 pass: entry.client_secret,
				 sendImmediately: true
			}
		}

		var idpCodeExchangeTimer = idp.stats.codeExchange.start();
		var codeExchangeTimer    = self.stats.codeExchange.start();

		options.form = body;
		try {
			request.post(idp.token_uri(req), options, function(err, response, data) {
				idpCodeExchangeTimer.end();
				codeExchangeTimer.end();

				if (data == null) {
					data = {
				    	error             : 'server_error',
				     	error_description : 'no data received from authorization server'
				  	};
				  	log.warn("no data received from authorization server for code exchange request: " + err);
				  	entry.error = data;
			  	} else {
				  	try {
					  	data = JSON.parse(data);
					  	if (data.access_token) {
					  		// do not write tokens to log.
						    log.info("received successful authz callback");
							entry.tokens  = data;
					  	} else {
					  		log.info("received error authz callback: " + JSON.stringify(data));
							entry.error   = data;
					  	}
				  	} catch (err) {
						log.warn("remote AS sent malformed response to token request: " + err);
					  	entry.error = {
							error: 'temporarily_unavailable',
							error_description: 'malformed data received from remote authorization server'
						};
				  	}
			  	}
			  	if (entry.error) {
			  		idp.stats.codeExchangeError.mark();
					self.stats.codeExchangeError.mark();
			  	}
			  	self.authzCompleted(entry);
			  	cb((entry.error) ? entry.error.error : null, entry);
			});
		} catch (e) {
			idpCodeExchangeTimer.end();
			codeExchangeTimer.end();

			idp.stats.codeExchangeError.mark();
			self.stats.codeExchangeError.mark();

			log.error("authz code to token exchange failed: " + e.message);
			entry.error = { error: 'server_error', error_description: e.message};
			self.authzCompleted(entry);
			cb(entry.error.error,entry);
		}
	});
};

/**
* Mark the entry as completed. Notify the callback once done.
*/
OAuth2DeviceAuth.prototype.authzCompleted = function(entry) {
	entry.completed();
	this.entry_storage.update(entry);
	this.audit.auth_completed(entry, (entry.error) ? 'failed' : 'success', null);
	idp.stats.authorizationComplete.mark();
	this.stats.authorizationComplete.mark();
};

/**
* Mark the entry as "in progress" i.e that user has entered correct user code
* and has been sent to the IdP / AS for authorization and we are
* waiting for the callback result from the AS.
*
* During this time, the device app can poll status until the
* entry expires or callback response is received from the AS.
*/
OAuth2DeviceAuth.prototype.entryInProgress = function(entry) {
	entry.inProgress();
	this.entry_storage.update(entry);
	this.audit.auth_in_progress(entry, 'success', null);
};

module.exports = OAuth2DeviceAuth;
