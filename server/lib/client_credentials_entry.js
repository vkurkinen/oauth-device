
/**
* Represents device client credentials entry that is stored
* to temporary cache for the duration of the authz transaction.
*/
function ClientCredentialsEntry() {
	this.client_secret   = null;
	this.idp_key   		 = null;
}; 

/**
* Encrypt payload of this entry and return it as string.
*/
ClientCredentialsEntry.prototype.encrypt = function(crypter) {
	// for now, only client secret.
	return crypter.encrypt(this.idp_key, this.client_secret);
}

/**
* Decrypt the given encrypted payload and update this objects state accordingly.
*/
ClientCredentialsEntry.prototype.decrypt = function(encrypted_data, crypter) {
	this.client_secret = crypter.decrypt(this.idp_key, encrypted_data);
}
module.exports = ClientCredentialsEntry;
