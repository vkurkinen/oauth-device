/**
* Utilities for generating PIN codes, tokens, UUIDs etc.
*/
var Chance = require('chance');
var chance = new Chance();
var uuid   = require('node-uuid');

/**
* Create new random string suitable to be used as an authorization kind of code.
*/
exports.code = function() {
	return chance.string({pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789','length': 32});
};


/**
* Create new random string suitable to be used as access/refesh token.
*/
exports.token = function() {
	return chance.string({pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789','length': 48});
};

/**
* Create a new 6 digit PIN code.
*/
exports.pin = function() {
	return chance.string({pool: '0123456789', 'length': 6});
};

/**
* Create a new UUID.
*/
exports.uuid = function() {
	return uuid.v4();
};

/**
* Create new transaction / entry ID with the given prefix.
*/
exports.txid = function(prefix) {
	var p = prefix || '';
	var id = chance.string({pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789','length': 24});
	return prefix + id; 
};
