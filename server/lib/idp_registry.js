/**
* Implements an IdP / AS registry. Each registry entry
* contains information about the IdP/AS, i.e. URLs that should be used
* in each needed scenario.
*/
var log           = require('log4js').getLogger("idp_reg");
var _             = require('underscore');
var crypto        = require('crypto');
var querystring   = require("querystring");

var metrics       = require('./metrics');

function IdPRegistry(config, url_shortener) {
	this.idps = {};
	this.self = this;
	this.config = config;
}

/**
* Adds an IdP/AS entry to the registry.
* Injects utility functions to each IdP to be used at runtime
* when resolving real URLs where to send the user.
*/
IdPRegistry.prototype.register_idp = function(idp_key, idp) {
	log.info("registering idp '" + idp_key + "'");

	idp.metrics = metrics.getIdPCollection(idp_key);

	idp.key = idp_key;
	if (!idp.state_encryption_key) {
		idp.state_encryption_key = this.config.get('state_encryption_key');
	}
	if (!idp.state_encryption_alg) {
		idp.state_encryption_alg = 'aes256';
	}
	if (!idp.client_credentials_send_method) {
		idp.client_credentials_send_method = this.config.client_credentials_send_method;
	}

 	// make 'idp' object with some helper functions based on config
	idp.callback_uri = function(req) {
		return idp.user_main_url(req,false) + "/user/cb";
	};

    // generate the verification URI. Async mode to allow more dynamic ways if needed.
	idp.verification_uri = function(req, callback) {
		callback(idp.user_main_url(req,true) + "/user");
	};

	idp.authorize_uri = function(req) {
		return idp.base_url + idp.authorize;
	};

	idp.token_uri = function(req) {
		return idp.base_url + idp.token;
	};

	idp.user_main_url = function(req, includeLang) {
		var url = req.ns.base_url(req) + '/oauth/u/';
		var lang = req.locale;
		if (lang && includeLang) {
			url +=  lang + '/';
		}
		return  url + req.ns.key + "/" + idp_key;
	}

	idp.issuer = function() {
		return idp.base_url;
	}

	// data encryption, possibly by idp specific keys
	idp.encode_data = function(data) {
		var cipher = crypto.createCipher(idp.state_encryption_alg, idp.state_encryption_key);
		var encrypted = cipher.update(JSON.stringify(data), 'utf8', 'hex') + cipher.final('hex');
   	    return new Buffer(encrypted).toString();
	}

	idp.decode_data = function(data) {
		var decoded = null;
		try {
			var decipher = crypto.createDecipher(idp.state_encryption_alg,idp.state_encryption_key);
  			decoded = decipher.update(data,'hex','utf8') + decipher.final('utf8');
		} catch (err) {
			log.warn('malformed encrypted data, cannot decrypt: ' + err.toString());
		}
		return JSON.parse(decoded);
	}


	// metrics
	idp.stats = metrics.newIdPStats(idp_key);

	this.idps[idp_key] = idp;
	return idp;
};

/**
* Initialize the registy from IdPs defined in the provided configurations
* 'idps' element.
*/
IdPRegistry.prototype.init = function() {
	this.idps = this.config.get('idps');
	var registry = this;
	_.each(this.idps, function (idp, idp_key) {
		registry.register_idp(idp_key, idp);
	});
};

/**
* Find en IdP entry from the registry based on the IdP key.
*/
IdPRegistry.prototype.find = function(key) {
	return this.idps[key];
};


module.exports = IdPRegistry;