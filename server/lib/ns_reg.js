/**
* Namespace registry
*/
var log     = require('log4js').getLogger("ns_reg");
var _       = require('underscore');

function NamespaceRegistry(config) {	
	this.namespaces = {};
	this.self = this;
	this.config = config;
}

/**
* Adds an namespace entry to the registry. 
*/
NamespaceRegistry.prototype.register_namespace = function(ns_key, ns) {
	log.info("registering namespace '" + ns_key + "'");

	ns.key = ns_key;
	if (!ns.public_authority && this.config.get('public_authority')) {
		ns.public_authority = this.config.get('public_authority');
	}
	if (ns.public_authority && ns.public_authority.slice(-1) == '/') {
		ns.public_authority = ns.public_authority.substring(0, ns.public_authority.length - 1);
	}

	ns.base_url = function(req) {
		var base_url = ns.public_authority;
		if (!base_url || base_url == null) {
			base_url = req.protocol + "://" + req.get('host');
		}
		return base_url;
	}
	
	this.namespaces[ns_key] = ns;
	return ns;
};

/**
* Initialize the registy from namespaces defined in the provided configurations
* 'namespaces' element.
*/
NamespaceRegistry.prototype.init = function() {
	this.namespaces = this.config.get('namespaces');
	var registry = this;
	_.each(this.namespaces, function (ns, ns_key) {
		registry.register_namespace(ns_key, ns);
	});
};

/**
* Find a namespace entry from the registry based on the key.
*/
NamespaceRegistry.prototype.find = function(key) {
	return this.namespaces[key];
}; 

module.exports = NamespaceRegistry;