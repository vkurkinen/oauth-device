/**
* Middleware functions specific to the Device OAuth Gateway
*/

/**
* Find IdP entry from the internal registry based on request parameter. 
* This function is only used by the middleware when resolving the IdP before
* request is passed to actual app code. 
*
* If IdP is unknown, the request is rejected and middleware does not pass
* the request in the stack but it is cancelled.
*
* It is really bad request if IdP cannot be resolved.
*/
exports.idp2req = function(req,res,next, idp_context_key) {
	idp = req.app.get('auth_service').getIdP(idp_context_key);
  	if (!idp) {
		res.json(400, {error: 'invalid_request', error_description: 'unknown authorization server'});
	  	return;
  	}
  	req.idp = idp;
  	return next();	
};

/**
* Code endpoint takes "lang" parameter in request body. 
* In order to apply normal i18next validation to it, 
* inject request and add the language parameter to request
* query parameter map, before the request reaches 18next middleware.
*
* This allows nice and clean language parameter validation. 
*/
exports.lang2req = function(req,res,next) {
  if (req.body.lang) {
    req.query.lang = req.body.lang;
  }
  next();
} 

/**
* Find namespace entry from the internal registry based on request parameter. 
* This function is only used by the middleware when resolving the namespace before
* request is passed to actual app code. 
*
* If namespace is unknown, the request is rejected and middleware does not pass
* the request in the stack but it is cancelled.
*
* It is really bad request if namespace cannot be resolved.
*/
exports.ns2req = function(req,res,next, ns_key) {
 	ns = req.app.get('auth_service').getNamespace(ns_key);
  if (!ns) {
	  res.json(400, {error: 'invalid_request', error_description: 'unknown namespace'});
	 	return;
	}
  req.ns = ns;
  return next();	
};

/**
* extract client_id + client_secret to request either from 
* Basic authorization header or from request query or body.
*/
exports.clientCredentials = function(req,res,next) {    
    var authorization = req.headers.authorization;    
    if (authorization) {
    	var parts = authorization.split(' ');
    	if (parts.length == 2) {
    		var scheme = parts[0];
    		var credentials = new Buffer(parts[1], 'base64').toString();
  	        var index = credentials.indexOf(':');
  	        
  	        if ('Basic' == scheme && index > 0) {	  	        	
  	        	req.client_id = credentials.slice(0, index);
  		        req.client_secret = credentials.slice(index + 1);
  		        return next();
    		}
    	}
    }
    // accept credentials also in request data
	var client_id     = req.query.client_id     || req.body.client_id;
	var client_secret = req.query.client_secret || req.body.client_secret;	
	if (client_id) {
		req.client_id = client_id;
	}
	if (client_secret) {
		req.client_secret = client_secret;
	}
	return next();
} 

/**
* Resolve access token from request data and assign to request
* (mostly used by Mock AS)
*/
exports.access_token2req = function(req,res,next) {  
  var access_token = req.query.access_token || req.body.access_token;
  if (!access_token) {
    var authorization = req.headers.authorization;    
    if (authorization) {
      var parts = authorization.split(' ');
      if (parts.length == 2) {
        var scheme = parts[0];
        if (scheme === 'Bearer') {
          access_token = parts[1]
        }
      }
    }
  }
  
  if (access_token) {
    req.access_token = access_token;
  }
  return next();
}