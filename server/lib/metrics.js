var measured = require('measured');

/**
* Provides statistics and other metrics collections.
*/
var common = measured.createCollection("common");

var idpCollections = {};

var collections = [
	common
];

var getIdPCollection = function(idp_key) {
	var m = idpCollections[idp_key];
	if (!m) {
		m = measured.createCollection(idp_key);
		idpCollections[idp_key] = m;
		collections.push(m);
	}
	return m;
}

function addIdPStats(stats, c) {

	stats.deviceCodeRequested  		= c.meter('deviceCodeInitialize');
	stats.deviceCodeCompleted      	= c.timer('deviceCodeComplete');

	stats.deviceCodeInvalidRequest  = c.meter('deviceCodeInvalidRequest');

	stats.codePollUnknownCode    	= c.meter('deviceCodeInvalidCode');
	stats.codePollExpiredCode    	= c.meter('deviceCodeExpiredCode');
	stats.codePollUsedCode       	= c.meter('deviceCodeUsedCode');
	stats.codePoll         			= c.meter('poll');
	stats.codePollFlowControl      	= c.meter('pollFlowControl');

	stats.tokenRefresh              = c.timer('tokenRefresh');
	stats.tokenRefreshError         = c.meter('tokenRefheshError');

	stats.authorizationStart         = c.meter('authorizationInitialize');
	stats.authorizationComplete      = c.meter('authorizationComplete');
	stats.authorizationAccessDenied  = c.meter('authorizationAccessDenied');
	stats.authorizationExpired       = c.meter('authorizationExpired');
	stats.authorizationOtherError    = c.meter('authorizationOtherError');

	stats.codeExchange               = c.timer('codeExchange');
	stats.codeExchangeError          = c.meter('codeExchangeError');

	stats.userCodeSubmit            = c.meter('userCodeSubmit');
	stats.userCodeUnknown			= c.meter('userCodeUnknown');
	stats.userCodeError  			= c.meter('userCodeError');
	//stats.uniqueClients				= c.counter('uniqueClients');
}

exports.newIdPStats = function(idp_key) {
	var c = getIdPCollection(idp_key);
	var stats = {};
	addIdPStats(stats,c);
	// ADD MORE: nbr of unique IP / clients / client stats...
	return stats;
}

exports.newGlobalStats = function(idp_key) {
	var c = common;
	var stats = {};
	addIdPStats(stats,c);
	// ADD MORE: nbr of unique IP / clients / client stats...
	return stats;
}

exports.getIdPCollection    = getIdPCollection;
exports.getCommonCollection = function() { return common };
exports.getAllCollections   = function() { return collections };
