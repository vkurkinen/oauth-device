var DeviceAuthStatus = new Enum(['CREATED','IN_PROGRESS','COMPLETED','USED']);	 	

/**
* Represents device authorization entry in the Gateway.
*/
function DeviceAuthEntry(data) {
	
	this.device_code       = null;
	this.user_code         = null;
	this.interval          = -1;
	this.created_time      = null;
	this.expiry_time       = null;	
	this.last_access_time  = -1;
	this.completed_time    = null;
	this.used_time         = null;
	this.status            = DeviceAuthStatus.CREATED;
	this.authz_code        = null;
	this.tokens            = null;
	this.error             = null;
	this.idp_key           = null;             
	this.ns_key            = null;
	this.issuer            = null;
	this.locale            = null;
	this.client_id         = null;
	this.client_secret     = null;
	this.client_secret_ref = null;
	this.scope             = null;	
	this.id                = null;
	
	if (data) {
		this.device_code       = data.device_code;
		this.user_code         = data.user_code;
		this.interval          = data.interval;
		this.created_time      = data.created_time;
		this.expiry_time       = data.expiry_time;	
		this.last_access_time  = data.last_access_time;
		this.completed_time    = data.completed_time;
		this.used_time         = data.used_time;
		this.status            = data.status;
		this.authz_code        = data.authz_code;
		this.tokens            = data.tokens;
		this.error             = data.error;
		this.idp_key           = data.idp_key;
		this.ns_key            = data.ns_key;
		this.issuer            = data.issuer;
		this.locale            = data.locale;
		this.client_id         = data.client_id;
		this.client_secret     = data.client_secret;
		this.client_secret_ref = data.client_secret_ref;
		this.scope             = data.scope;	
		this.id                = data.id;	
	}
}; 

DeviceAuthEntry.prototype.completed = function() {
	var now = new Date().getTime();
    this.completed_time = now;
	this.status = DeviceAuthStatus.COMPLETED;
};

DeviceAuthEntry.prototype.used = function() {
	var now = new Date().getTime();
	this.last_access_time = now;
   this.used_time = now;
	this.status = DeviceAuthStatus.USED;
};

DeviceAuthEntry.prototype.inProgress = function() {
	var now = new Date().getTime();
	this.last_access_time = now;
	this.status = DeviceAuthStatus.IN_PROGRESS;
};

DeviceAuthEntry.prototype.touch = function() {
	var now = new Date().getTime();
	this.last_access_time = now;
};

DeviceAuthEntry.prototype.isCompleted = function() {
	return this.status == DeviceAuthStatus.COMPLETED;
};

DeviceAuthEntry.prototype.isUsed = function() {
	return this.status == DeviceAuthStatus.USED;
};

DeviceAuthEntry.prototype.isInProgress = function() {
	return this.status == DeviceAuthStatus.IN_PROGRESS || this.status == DeviceAuthStatus.CREATED;
};

DeviceAuthEntry.prototype.isExpired = function() {
	var now = new Date().getTime();
	return this.expiry_time < now;
};

DeviceAuthEntry.prototype.isDone = function() {
	return this.isCompleted() || this.isUsed() || this.isExpired();
};

DeviceAuthEntry.prototype.shouldSlowDown = function() {
	var now = new Date().getTime();
	var next_allowed =  this.last_access_time + (this.interval * 1000);	
	return next_allowed > now;
};

DeviceAuthEntry.prototype.hasTokens = function() {
	return this.tokens != null;
};

DeviceAuthEntry.prototype.getTokens = function() {
	if (!this.tokens) {
		return null;
	}
	var now = new Date().getTime();
	var tokens = this.tokens;
	if (tokens.expires_in > 0) {
		var exp_time = this.completed_time  + (tokens.expires_in * 1000);	
		tokens.expires_in = Math.floor((exp_time - now) / 1000);
	}	
	return tokens;
};

DeviceAuthEntry.prototype.getError = function() {
	return this.error;
};

module.exports = DeviceAuthEntry;
