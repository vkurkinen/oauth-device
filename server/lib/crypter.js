

/**
* Simple component for encrypting and decrypting string data
* using IdP specific functions.
*/
function Crypter(idp_registry) {   
    this.idp_registry = idp_registry;
}

Crypter.prototype.encrypt = function(idp_key, data) {
    return this.idp_registry.find(idp_key).encode_data(data);
}

Crypter.prototype.decrypt = function(idp_key, data) {
    return this.idp_registry.find(idp_key).decode_data(data);
}

module.exports = Crypter;