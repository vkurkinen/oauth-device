/**
* Provides middleware to handle CORS.
*/

var cors = require('cors');

var whitelist = /.+/g;
var options = {};

function configure(config) {
    options.maxAge = 86400;
}

function corsOptionsDelegate(req, callback) {
    var cors_options = { 
        maxAge      : options.maxAge, 
        methods     : 'GET,POST', 
        credentials : false 
    };

    var origin = req.header('Origin');

    var accept = allow_origin(origin);
    cors_options.origin = accept; 
    callback(null, cors_options); 
};

function allow_origin(origin) {
    return (whitelist.exec(origin) != null);
}

function handler() {
    return cors(corsOptionsDelegate)
}

exports.configure = configure;
exports.handler = handler;