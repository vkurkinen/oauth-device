
var assert = require('assert');
var should = require('should');
var jsdom  = require('jsdom');

var nock        = require('nock');
var request     = require('request');
var querystring = require("querystring");
var url         = require("url");

var codegen = require('../lib/codegen');
var app = require('../app.js');
var OAuth2DeviceClient = require('oauth2-device-client');

var http = require('http');
var log = require('log4js').getLogger('test');
var _ = require('lodash');


var test_idp_config = {
    name      : "test",
    base_url  : "http://localhost:6666",
    authorize : "/authz",
    token     : "/token"     
	};

var host = 'http://localhost:3000';
var test_scope = 'profile';
var namespace = 'local';

// tests are run against the 'local' namespace to ensure we are 
// communicating with the direct node app instead of front-end LB.
var auth_client = new OAuth2DeviceClient(host,namespace,'test', 'fscca','secret');

var auth_service = app.get("auth_service");
var test_idp = auth_service.registerIdP(test_idp_config.name, test_idp_config);

describe('Requesting', function() {
	describe('new device auth entry', function() {		
		it('with invalid authorization server key should be rejected as invalid request', function(done) {
		    this.timeout(2000);	    
            var bad_client = new OAuth2DeviceClient(host,namespace,'BADIDP', 'fscca','secret'); 
			bad_client.code('profile', null,  function (error,data) {
					should.exist(error);
					error.error.should.equal('invalid_request');
					done();
			});				
		});					
		it('with invalid namespace key should be rejected as invalid request', function(done) {
		    this.timeout(2000);
	        var bad_client = new OAuth2DeviceClient(host,'BAD_NS','test', 'fscca','secret'); 
     		bad_client.code('profile', null,  function (error,data) {
					should.exist(error);
					error.error.should.equal('invalid_request');
					done();
			});				
		});
		it('with malformed language key should fallback to default (english)', function(done) {
		    this.timeout(2000);
	        auth_client.code('profile', '#<script>alert();</script>',  function (error,data) {
					should.not.exist(error);
					should.exist(data);
					data.verification_uri.should.containEql('u/en/local/test');
					done();
			});				
		});					
	});
});

describe('Creating', function() {
	  
	describe('new device auth entry with client', function() {
		
		it('should succeed', function(done) {
		    this.timeout(15000);
		    
			function poll(entry, expected_error) {
				auth_client.token(entry.device_code, function (error,data) {					
						//log.debug("POLL: " + JSON.stringify(data));
						should.exist(error);
						should.not.exist(data);
						assert.equal(error.error,expected_error);
					});				
			}
			
			function poll_success(entry) {
				auth_client.token(entry.device_code, function (error,data) {
						//log.debug("POLL: " + JSON.stringify(data));
						should.not.exist(error);
						should.exist(data);
					});				
			}								
			
			function processDeviceCodeResponse(entry) {				
				poll(entry,'authorization_pending');
				setTimeout(function() { poll(entry,'slow_down'); }, 500);				
				setTimeout(function() { poll(entry,'authorization_pending'); }, 1100);				
				setTimeout(function() {  
				     done();
				     }, 1500);				
			};
			
			auth_client.code('profile', null,  function (error,data) {
				    //log.debug("ERROR: " + JSON.stringify(error));
					//log.debug("CODE: " + JSON.stringify(data));
					// really simple test for the code response
					should.not.exist(error);
					should.exist(data);
					assert.ok(typeof data.device_code == 'string');
					assert.ok(typeof data.user_code == 'string');
					assert.ok(typeof data.verification_uri == 'string');
					assert.ok(typeof data.expires_in == 'number');
					assert.ok(typeof data.interval == 'number');
					
					processDeviceCodeResponse(data);
			});				
		});					
	});
});

describe('Device OAuth2 flow', function(){
	  
	describe('with valid requests', function(){
		it('should succeed', function(done) {
		    this.timeout(15000);
		    
		    function device_poll(entry, expected_error) {
		    	auth_client.token(entry.device_code, function (error, data) {				
						//log.debug("POLL: error -> " + JSON.stringify(error) + ", data -> " + JSON.stringify(data));
						assert.equal(error.error,expected_error);
					});
			}
		    
			function device_poll_success(expected, entry) {
				log.debug("DEVICE POLL SUCCESS FOR DATA: " + JSON.stringify(entry));
				auth_client.token(entry.device_code, function (error, data) {				
						log.debug("POLL SUCCESS: " + JSON.stringify(data));
						if (error) {
						   assert.fail('expected success from poll, but got error: ' + JSON.stringify(error));
						}
						data.access_token.should.equal(expected.access_token);
						data.refresh_token.should.equal(expected.refresh_token);
						data.token_type.should.equal(expected.token_type);
						data.expires_in.should.be.type('number');																	
					});				
			}
			
			function get_user_code_page(entry, callback) {
				// first get the user code page
				http.get(host + '/oauth/u/' + namespace +'/test/user', function (res) {
					res.setEncoding('utf8');
					res.on('data', function (data) {						
						//log.debug("GET USER: " + data);
						// for now, just check that there is valid page returned...
						jsdom.env(data, ['../node_modules/jquery/lib/node-jquery.js'], 
								  function(errors, window) {
							           //assert.ok(!errors, errors);
							           // send the code 							           
							           callback(entry);							           
								  });					 						    	
					});				
				});
			}
								
			function send_user_code(entry) {				
				//log.info("send_user_code: " + entry.user_code);				
				var options = { followRedirect: false, form : {code: entry.user_code} };
				request.post(host + '/oauth/u/' + namespace + '/test/user', 
						  options, function(err, response, body) {								      
							  response.statusCode.should.equal(302);					      
							  var authz_url = url.parse(response.headers.location, true);
							  var authz_data = authz_url.query;

							  authz_data.scope.should.equal(test_scope);
							  authz_data.client_id.should.equal(auth_client.client_id);
							  authz_data.state.should.not.be.empty;
							  authz_data.redirect_uri.should.not.be.empty;
							 
							  authz_data.code = create_authz_code(authz_data, entry);
							 							  							  
							  var q = 'code=' + authz_data.code + '&state=' + authz_data.state;							  						
							  var tokens = mock_authz_code_request(authz_data, entry);							  							  
							  							  
							  var idp_token_host = test_idp_config.base_url;							  
							  // TODO: check Authz Basic for client_id:client_secret... + body data
							  var authz_code_request = nock(idp_token_host).							                                
							                                log(console.log).
							                                persist().
							                                post(test_idp_config.token).
							                                reply(200, tokens);

							  // invoke callback in redirect_uri
							  //log.info("INVOKE CALLBACK: " + authz_data.redirect_uri + "?" + q);
							  request.get(authz_data.redirect_uri + '?' + q, function (err, response, body) {								  								  
							  });				
							  
							  // after post has been made, the tokens should be available.
							  setTimeout(function() { device_poll_success(tokens,entry); }, 1500);
							  setTimeout(function() { device_poll(entry,'invalid_grant'); done(); }, 2000);							 
						  });
			}
			function create_authz_code(authz_data, entry) { 
				return codegen.token();								
			}
			
			function mock_authz_code_request(authz_data, entry) {							
				var access = codegen.token();
				var refresh = codegen.token();				
				var tokens = {
						access_token: access,
						refresh_token: refresh,
						expires_in: 1800,
						token_type: 'Bearer'
				};
				return tokens;
			}
						
			function processDeviceCodeResponse(entry) {				
				device_poll(entry,'authorization_pending');
				setTimeout(function() { device_poll(entry,'slow_down'); }, 500);
				setTimeout(function() { get_user_code_page(entry, send_user_code);}, 1500);
			};
			
			auth_client.code(test_scope, null, function (error,data) {
				//log.debug("CODE: " + JSON.stringify(data));
				processDeviceCodeResponse(_.cloneDeep(data));
			});
								
		});
	}); 
});

describe('Device OAuth2 flow client credentials', function(){
	var code_url      = host + '/oauth/device/' + namespace + '/test/code';
	var client_id     = auth_client.client_id;
	var client_secret = auth_client.client_secret;
	
	function assertClientCredentials(err, resp, data) {
		var device_code = JSON.parse(data).device_code;
		should.exist(device_code);
		auth_service.getEntryByDeviceCode(test_idp, device_code, function(error, entry) {
		   entry.client_id.should.equal(client_id);
		   // client_secret should not be returned in this operation.
		   should.not.exist(entry.client_secret);
		});
	}
	
	describe('as Basic Authorization header', function(){
		it('should be read successfully', function(done) {
		    var body = { response_type : 'device_code', scope : test_scope};			  
		    var options = { followRedirect: true, form : body };		     
	    	options.auth = {
	    			  user: client_id,
	    			  pass: client_secret,
	    			  sendImmediately: true
	   				};	     	    	
		    request.post(code_url, options, assertClientCredentials);
		    setTimeout(done(), 1000);
		});
	});
	
	describe('as FORM parameters', function(){
		it('should be read successfully', function(done) {
		    var body = { response_type : 'device_code', scope : test_scope, 
		    	         client_id: client_id, client_secret: client_secret};			  
		    var options = { followRedirect: true, form : body };		   
		    request.post(code_url, options, assertClientCredentials);
		    setTimeout(done(), 1000);
		});
	});
	
	describe('as QUERY parameters', function(){
		it('should be read successfully', function(done) {
		    var body = { response_type : 'device_code', scope : test_scope};			  
		    var options = { followRedirect: true, form : body };		   
		    request.post(code_url + '?client_id=' + client_id + '&client_secret=' + client_secret, options,
		    		     assertClientCredentials);
		    setTimeout(done(), 1000);
		});	
	});
});
