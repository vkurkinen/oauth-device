var assert = require('assert');
var Chance = require('chance');
var log4js = require('log4js');
log4js.configure('log4js.json');

var chance = new Chance();

var oauth2_device_auth = require('../oauth2_device_auth');

var config = {'name': 'LOCAL'};
var log = log4js.getLogger('unit-test');
var auth_service = new oauth2_device_auth(config, log4js);

// TODO: Check how to split tests into multiple files and how to run unit and integration tests separately?
describe('Reading', function(){
  
	describe('IdP Config \"local\"', function(){
		it('should have local config', function(done) {
			idp = auth_service.getIdP('local');
			assert.equal('LOCAL', idp.name);
			//assert.equal('https://localhost:9031', idp.base_url);
			done();
		});
	}); 
});