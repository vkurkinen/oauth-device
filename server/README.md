Device Authorization Gateway for OAuth2 
=======================================

This application provides integration logic to connect Smart TV and similar kind of 
applications to perform delegated user authentication and authorization process 
for user consent for applications using a separate device than the one where 
the application is running. 

It is suitable for applications that run on devices that have decent UI to show 
information to user, but limited input capabilities so that it is not possible, 
or convenient enough, for user to perform full authentication and application 
authoriztion flow directly in the device the application is running. 
An example application might be content carousel application running in a 
set-top box showing users content from a cloud service in a TV screen. 

For this kind of applications to properly perform user authentication from an 
identity provider with any methods the user wants to use, and possibly request 
authorization consent from the user for accessing users profile or other data 
in the cloud, a separate module is needed that connects user authentication flow 
used from one device to the application running in another device. 

This integration is provided by the Device OAuth2 Gateway i.e. this appliation. 

The model used by this gateway is close to the original Device Flow proposed in **RFC 6749** 
drafts initially and what has been implemented by Facebook and Google. 
Refer to following links for more background information:

* [OAuth2 RFC draft-06](https://tools.ietf.org/html/draft-ietf-oauth-v2-06#section-2.7)
* [Facebook Using Login With Devices](https://developers.facebook.com/docs/facebook-login/using-login-with-devices/)
* [Google OAuth 2.0 For Devices](https://developers.google.com/accounts/docs/OAuth2ForDevices)

The gateway supports only OAuth2 and utilizes *Authorization Code Grant* method with the authorization servers
it interacts with.

For more information about this application, check out: 

* [Design](./doc/DESIGN.md)
* [HTTP API](./doc/DEVICE_AUTH_API.md) (or [Client SDK](../sdk/README.md))
* [Server Configuration](./doc/CONFIGURATION.md)