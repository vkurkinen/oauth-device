Device Authorization Gateway for OAuth2 
=======================================

This repository contains OAuth2 Device Gateway server application 
that can be used to perform delegated authentication and authorization 
for applications running in another device via some other device. 

See [OAuth2 Device Authorization Gateway](./server/README.md) for mode details about
the server application. 

There is also a simple [demo client application](./demo/README.md) that uses the gateway to delegate authorization into 
separate device. 

A [client SDK](./sdk/README.md) module also exists for easy usage of the gateway,

**NOTE**: This project is very much work in progress so not ready to be used as is, be warned...