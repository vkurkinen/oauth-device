/*
 * GET home page.
 */
var log4js = require('log4js');
var log = log4js.getLogger('app');


exports.index = function(req, res){
  	if (req.session && req.session.logged_in) {
	    res.redirect('./app');
  	} else {
	    res.render('index');
  	}
};

exports.appmain = function(req, res){	
    res.render('appmain', { user    : req.session.user, 
    	                    tokens  : req.session.tokens,
                            issuer  : req.session.tokens.issuer,
    	                    client  : req.session.client
    	                  });
};

exports.logout = function(req, res){
	if (req.session) {
		req.session.destroy();
	}
	log.info('logged out, rendering index page');
	res.redirect(req.app.webroot);
};


