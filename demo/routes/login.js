var log4js = require('log4js');
var log    = log4js.getLogger('app');
var config = require('../lib/config');
var qr     	= require('qr-image');

var URLShortener = require("../lib/url_shortener");
var OAuth2DeviceClient = require('oauth2-device-client');
var TokenValidator = require('../lib/token_validator');

var url_shortener = new URLShortener(config.get("shortener"));

var default_server_key 	  = config.get('active_as');
var ns_key                = config.get('namespace');

var auths = {};

function get_auth(server_key) {
	var auth = auths[server_key];
	if (!auth) {
		auth = create_auth(server_key);
		if (auth != null) {
			auths[server_key] = auth;
		}
	}
	return auth;
}

function create_auth(server_key) {
	var authorization_server = config.get('authorization_servers')[server_key];
	if (!authorization_server) return null;

	var client = new OAuth2DeviceClient(authorization_server.url, ns_key,
		                                            server_key, 
		                                            authorization_server.client_id, 
		                                            authorization_server.client_secret);

	// add listeners to some interesting events, mostly for debugging and logging purposes.
	client.on('data', function (e) {
		log.debug("device authz event: " + JSON.stringify(e));
	});
	client.on('tokens', function (e) {	
		log.info("device auth successfully done for code " + e.ref);
	});
	client.on('server_error', function (e) {	
		log.info("device auth poll failure for code " + e.ref);
	});

	var token_validator = new TokenValidator(authorization_server.validate_url, 
											 authorization_server.type,
		                                     authorization_server.validate_client_id, 
		                                     authorization_server.validate_client_secret); 
	return { client: client, server: authorization_server, validator : token_validator };
}


exports.login = function(req, res){
	log.info("initiating login...");
	var server_key = req.query.as || default_server_key;
	var auth = get_auth(server_key);
	if (auth == null) {
		res.send(400, 'Unkown Authorization Server Key');
		return;
	}
	auth.client.code(auth.server.scope, req.locale, function (error, data) {		
		var model = {};
		if (error) {
			model.error   = error.error;
		} else {
			var url = data.verification_uri;
			if (url.indexOf('?') > 0) {
				url += '&';
			} else {
				url += '?';
			}
			url += 'code=' + data.user_code;
			// shorten the visible verification URI to demo this option also
			// the actual verification_uri from server is direct so  that it can 
			// be appended with the user code for quick access (like QR code)
		    url_shortener.shorten(data.verification_uri, function (short_uri) { 
				model.user_verification_url  = url;
				model.verification_uri 	     = data.verification_uri;
				model.short_verification_uri = short_uri;
				model.short_uri_available    = (short_uri.length < data.verification_uri.length);
				model.user_code              = data.user_code;
				model.interval         	     = data.interval + 1;
				model.expires_at       	     = new Date(new Date().getTime() + data.expires_in * 1000);			
				model.qr_url           	     = req.app.webroot + '/login/qr.png';

				req.session.server_key 					   = server_key;
				req.session.authz_tx 		               = data;
				req.session.authz_tx.user_verification_url = model.user_verification_url;
				req.session.authz_tx.expires_at       	   = model.expires_at;
				log.info("requesting user to authorize with: " + JSON.stringify(model));
           		       res.render('login', model);
			});
		}
	});
};

exports.poll = function(req, res){	
	
	log.info("processing login status poll... ");
		
	if (!req.session || !req.session.authz_tx) {
		log.warn("no device code in session, returning as expired");
		res.json(200, { state : 'invalid_grant' });
		return;
	}	
	
	var server_key = req.session.server_key || default_server_key;
	var auth = get_auth(server_key);
	if (auth == null) {
		res.send(400, 'Unkown Authorization Server Key');
		return;
	}
	
	auth.client.token(req.session.authz_tx.device_code, function(error, tokens) {		
		if (error) {
			switch (error.error) {
				case 'authorization_pending':
				case 'slow_down':
					log.info("authorization still in progress...");
					break;
				case 'invalid_grant':					
				   log.warn("code seems expired or used, removing local state: " + JSON.stringify(error));
				   req.session.authz_tx = null;
				   break;
				default:
					log.error("received error for login poll with device code " + JSON.stringify(error));					
			}			
			res.json(200, { state : error.error });
			return;
		} 
		// else we have tokens, need to fetch user profile
		req.session.tokens = tokens;
		// throw away the code as it is of no good anymore.
		req.session.authz_tx = null;		
		
		auth.validator.validate(tokens, function(error, validation_result) {
			var state = null;
			if (error) {
				log.error("token validation failed: " + JSON.stringify(error));
				state = 'server_error';
			} else {
				var now = new Date().getTime();
				log.info("token successfully validated: " + JSON.stringify(validation_result));
				req.session.user      = validation_result.user;
				req.session.client    = { 
					client_id   : validation_result.client_id,
					expiry_time : new Date(now+validation_result.expires_in * 1000) 
				};
				req.session.validation_result = validation_result;
				req.session.logged_in = true;
				state = 'success';
			}			
			res.json(200, { state : state });
		});						
	});
};


exports.qr = function(req, res){	
	if (!req.session || !req.session.authz_tx) {
		res.send(404, 'Not Found');
		return;
	}	
    try {
        var img = qr.image(req.session.authz_tx.user_verification_url);
        res.writeHead(200, {'Content-Type': 'image/png'});
        img.pipe(res);
    } catch (e) {
        log.error('qr : failed -> ' + JSON.stringify(e));
        res.send(400, 'Bad Request');
    }	
};
