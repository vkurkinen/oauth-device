/*
 * GET home page.
 */
var log4js = require('log4js');
var log = log4js.getLogger('app');

exports.index = function(req, res){	
    res.render('user', { "user"   : req.session.user, 
                         "tokens" : req.session.tokens,
                         "issuer" : req.session.tokens.issuer,
                         "client" : req.session.client,
                        });
};


