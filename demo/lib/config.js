/**
* Reads configuration files from the "conf" directory and applies
* system and deployment overrides, if available.
*
* Uses the "nconf" module for actual reading and merging of configuration settings
* from different files. 
* 
* Final config is exported as an instance of "nconf".
*/
var fs    = require('fs');
var nconf = require('nconf');
var _ = require('underscore');
//
// 1. any overrides
//
nconf.overrides({
});

//
// 2. `process.env`
// 3. `process.argv`
//
nconf.env().argv();

//
// 4. Values in `config-XXX.json` files
//
var files = fs.readdirSync('/etc/oauth2dev-demo.d');
_.each(files, function(f) {
	if (f.indexOf("config-") == 0) {
		nconf.file(f,'/etc/oauth2dev-demo.d/' + f);
	}
});

//
// Additional local override
//
nconf.file('local', '/etc/oauth2dev-demo.d/local.json');
	
//
// Additional deployment override
//
nconf.file('deploy', '/etc/oauth2dev-demo.d/deploy.json');

//
// 5. Any default values
//
nconf.defaults({

});

module.exports = nconf;