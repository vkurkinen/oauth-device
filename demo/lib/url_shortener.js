/**
* Implements simple URL shortening service using an external global service and internal 
* caching of shortened URLs.
*
* Main use is shortening the verification_uri information provided to clients. 
*
* Now supports only Bitly. 
*
* Shortened URLs are cached internally for some time to avoid calling external systems
* too often with the same data as the verification URIs are often same and contain only a 
* a few variable parts.
*/
var log         = require('log4js').getLogger("url_shortener");
var Bitly       = require("bitly-oauth");
var NodeCache   = require("node-cache");

/**
* Create new URL shortener with provided configuration.
*
* Configuration looks like:
*
{
    "enabled" : true | false,
    "bitly"   : {
	  "username" : <bitly-user>,
	  "password" : <bitly-password>
    },
    "cache" : {
	   "ttl": <time in secs to cache shortened URLs>
    }
} 

* If shortening is not enabled, the URLs are returned as is. 
* Also, if shortening fails, the original URL is returned as is.
*/
function URLShortener(config) {	
	this.self = this;
	this.shortener;
	this.enabled = config && config.enabled;
    
    if (!this.enabled) {
    	log.info("URL shortening not enabled: config -> " + JSON.stringify(config));
    	return;
    }

	if (config.bitly) {  
       shortener = new BitlyShortener(config.bitly);
       log.info("URL shortening with Bitly enabled");

	}
	this.cache = new NodeCache( { stdTTL: config.cache.ttl, checkperiod: 600 } );
}

/**
* Shorten the URL. Calls the given callback function with the shortened URL
* once it is available. If shortening fails or is not enabled, the callback is called
* with the original URL.
*/
URLShortener.prototype.shorten = function(url, callback) {
	if (!this.enabled) {
		callback(url);
		return;
	}
	var cache = this.cache;
	var cache_entry = cache.get(url);
	if (cache_entry && cache_entry[url]) {
		var short_url = cache_entry[url];
		log.debug("using short URL from cache: " + url + " -> " + short_url);
		callback(short_url);
	} else if (shortener) {
		log.info("shortening verification URI " + url);
		shortener.shorten(url, function (result) {
			cache.set(url, result);
			callback(result);
		})
	} else {
		callback(url);
	}
};

/**
* Bitly shortening wrapper, user internally only.
*/
function BitlyShortener(config) {
    this.bitly = new Bitly(config.username, config.password);  
}

BitlyShortener.prototype.shorten = function(long_url,callback) {
   log.debug("shortening verification URI from bitly...");
   this.bitly.shorten({longUrl: long_url}, function (err, result) {
       var short_url = long_url;
	   if (err) {
	   	  log.warn("verification URI shortening with bitly failed (using original URL): " + JSON.stringify(err));
	   } else if (!result || !result.data.url) {
	       log.warn("verification URI shortening with bitly failed (using original URL): " + JSON.stringify(result.data));	
	   } else {
	   	   short_url = result.data.url;
       	   log.debug("shortened verification URI: " + long_url + " -> " + short_url);
	   }
	   callback(short_url);
	});
}

module.exports = URLShortener;