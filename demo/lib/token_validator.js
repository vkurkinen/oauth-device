/**
* Validates OAuth2 access tokens issued by a selected token validator server.
* 
* Fetches token validation data from the defined server 
* token validation endpoint with the given validation client credentials
* and passest the request to a given callback function for further processing.
*/
var request = require('request');
var merge   = require('merge');
var log     = require('log4js').getLogger('token_validator');

/**
* Create new token validator with given information.
* @param rofile_url PingFederate token endpoint.
* @param client_id Token validation client ID.
* @param client_secret Token validation client secret.
*/
function TokenValidator(profile_url, type, client_id, client_secret) {		
    // options applied to all requests.
    this.common_request_options = { strictSSL: false };

    this.validator = null;
    switch (type) {
        case 'pingfederate':
            this.validator = this.validate_pingfederate;
            break;
        case 'openid':
            this.validator = this.openid_connect_userinfo;
            break;            
        case 'googleplus':
            this.validator = this.validate_google;
            break;            
        default:
            throw Error('unsupported token validator type: ' + type);
    }

	this.profile_url   = profile_url;	
	this.client_id     = client_id;
	this.client_secret = client_secret;
    if (client_id && client_secret) {
    	this.auth_creds = {
    			user: client_id,
    			pass: client_secret,
    			sendImmediately: true
    	}; 		
    }
}

/**
* Perform token validation for the given access token and call the
* provided callback function with the result.
* 
* Callback gets two arguments: 'error' and 'data'. Error is null if the validation 
* request was successfully executed and the AS did not return error response (i.e. token is valid) . 
* In such case 'data' is set to the JSON object returned by the AS and it contains
* the token validation data.
*
* If request processing fails, or remote AS returns error response (e.g. token is not valid),
* then data is null and first argument is the JSON error object. 
* 
*/
TokenValidator.prototype.validate = function (tokens, callback) {
    this.validator(tokens, function(error,data) {
        if (error) {
            log.warn("token validation from " + this.type + " failed: " + JSON.stringify(error));
        } else {
            log.warn("token validation done from " + this.type + ": " + JSON.stringify(data));
        }
        callback(error,data);
    });
};

TokenValidator.prototype.validate_google = function(tokens, callback) {
    var options = {};
    options.headers = {
        "Authorization" : 'Bearer ' + tokens.access_token
    };
    options = merge(this.common_request_options, options);

    var client_id = this.client_id;
    request.get(this.profile_url, options, function(err,resp,data) {
        if (err) {
            handle_fetch_error(err, callback);
            return;
        }
        if (resp.statusCode == 200 && data) {            
            try {
                data = JSON.parse(data);
                if (data.error) {
                    callback(data, null);
                } else {
                    // make common response
                    var v = {
                        data       : data,
                        expires_in : tokens.expires_in,
                        client_id  : client_id,
                        user       : {
                            displayName : data.displayName,
                            id          : data.id,
                            givenName   : data.name.givenName,
                            familyName  : data.name.familyName,
                        }
                    };
                    resolve_displayName(v.user);
                    callback(null, v);
                }
            } catch (e) {
                log.error("failed to parse token validation response from AS: " + e + ", data -> " + data);
                callback({ error: 'server_error', error_description: 'malformed data received from remote server'}, null);    
            }
        } else {
            log.error("failed to validate token, got " + resp.statusCode + " " + data);
             if (data) {
                callback(data, null);
            } else {
                callback({ error: 'server_error'}, null);
            }
        }
    }); 
};

TokenValidator.prototype.openid_connect_userinfo = function(tokens, callback) {
    var profile_url = this.profile_url;
    var options = { followRedirect: true };
    options = merge(this.common_request_options, options);

    if (this.auth_creds) {
        options.auth = this.auth_creds;
        // we cannot set Beader authorization header, so include token as query param
        if (profile_url.indexOf('?') > 0) {
            profile_url += "&";
        } else {
            profile_url += "?";
        }
        profile_url += "access_token=" + tokens.access_token;
    } else {
        options.headers = {
         "Authorization" : 'Bearer ' + tokens.access_token
        };
    }
    
    request.get(profile_url, options, function(err,resp,data) {
        if (err) {
            handle_fetch_error(err, callback);
            return;
        }

        if (resp.statusCode == 200 && data) {
            try {
                data = JSON.parse(data);
                if (data.error) {
                    callback(data, null);
                } else {
                    var v = {
                        data       : data,
                        expires_in : data.expires_in,
                        client_id  : data.client_id,
                        user       : {
                            username     : data.preferred_username,
                            displayName  : data.name,
                            email        : data.email,
                            id           : data.sub,
                            givenName    : data.given_name,
                            familyName   : data.family_name,
                        }
                    };
                    resolve_displayName(v.user);
                    callback(null, v);
              }
            } catch (e) {
                log.error("failed to parse user info response from AS: " + e + ", data -> " + data);
                callback({ error: 'server_error', error_description: 'malformed data received from remote server'}, null);    
            }
        } else {
            log.error("failed to fetch user info, got " + resp.statusCode + " " + data);
            if (data) {
                callback(data, null);
            } else {
                callback({ error: 'server_error'}, null);
            }
        }
    }); 
};

TokenValidator.prototype.validate_pingfederate = function(tokens, callback) {
    var body = {};
    body.grant_type = 'urn:pingidentity.com:oauth2:grant_type:validate_bearer';
    body.token = tokens.access_token;
    
    var options = { followRedirect: true, form : body };
    if (this.auth_creds) {
        options.auth = this.auth_creds;
    }
    options = merge(this.common_request_options, options);

    request.post(this.profile_url, options, function(err,resp,data) {
        if (err) {
            handle_fetch_error(err, callback);
            return;
        }
        if (resp.statusCode == 200 && data) {
            try {
                data = JSON.parse(data);
                if (data.error) {
                    callback(data, null);
                } else {
                    // make common response
                    var v = {
                        data       : data,
                        expires_in : data.expires_in,
                        client_id  : data.client_id,
                        user       : {
                            displayName  : data.access_token.displayName,
                            id           : data.access_token.uuid,
                            givenName    : data.access_token.givenName,
                            familyName   : data.access_token.familyName,
                            username     : data.access_token.userName,
                            email        : data.access_token.email
                        }
                    };
                    resolve_displayName(v.user);
                    callback(null, v);
              }
            } catch (e) {
                log.error("failed to parse token validation response from AS: " + e + ", data -> " + data);
                callback({ error: 'server_error', error_description: 'malformed data received from remote server'}, null);    
            }
        } else {
            log.error("failed to validate token, got " + resp.statusCode + " " + data);
            if (data) {
                callback(data, null);
            } else {
                callback({ error: 'server_error'}, null);
            }
        }
    }); 
};

function resolve_displayName(user) {
    if (user.displayName && user.displayName != null && user.displayName.trim().lenght > 0) {
        return;
    }

    user.displayName = user.givenName + " " + user.familyName;
    user.displayName = user.displayName.trim();
    
    if (user.displayName.length >= 0) {
        return;
    }

    if (user.username != null || user.username.length > 0) {
        user.displayName = user.username;
    } else if (user.email != null && user.email.length > 0) {
        user.displayName = user.email; 
    }
}

function handle_fetch_error(err, callback) {
    log.error("failed to fetch user data with token: " + err);
    callback({ error: 'server_error',
               error_description: 'unexpected error when communicating with the user profile server: ' + err.message
             }, null);
}

module.exports = TokenValidator;