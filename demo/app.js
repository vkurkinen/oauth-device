
var express = require('express');
var route_main = require('./routes/main');
var route_login = require('./routes/login');
var route_user = require('./routes/user');
var http    = require('http');
var path    = require('path');
var i18next = require('i18next');

var app = express();
module.exports = app;

var configuration = require('./lib/config');

var log4js = require('log4js');
log4js.configure('/etc/oauth2dev-demo.d/log4js.json', configuration.get('logging'));
var log = log4js.getLogger("app");

var RedisStore = require('connect-redis')(express);
var redis = require("redis");

// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser());

app.use(express.session({
			key: "sid", 
			secret: configuration.get('session_secret'),
			proxy: true,
			store: new RedisStore(configuration.get('redis')),
			cookie: {
				maxAge: 3600000,
				secure: false
				}
			}));

app.use(function(req,res,next) {
	req.log = log;
	return next();
});

var i18next_options = {
		 lng                  : 'en',
		 supportedLngs        : ['en-US','en', 'fi'],
		 detectLngFromPath    : 0,
		 detectLngQS          : 'lang',
		 cookieName           : 'lang',
		 useCookie            : true,
		 detectLngFromHeaders : true, 
		 fallbackLng          :  'en',
		 resGetPath           : 'locales/__ns__-__lng__.json',
		 ns			          : 'messages'
	};

i18next.init(i18next_options);
i18next.registerAppHelper(app);
app.use(i18next.handle);

app.use(app.router);

webroot = "/o2devdemo";
app.webroot = webroot;
app.locals.webroot = webroot;


app.use(express.static(path.join(__dirname, 'public')));	


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/'          , route_main.index);
app.get(webroot  + '/'          , route_main.index);
app.get(webroot  + '/app'      , loginControl, route_main.appmain);
app.get(webroot  + '/app/user' , loginControl, route_user.index);
app.get(webroot  + '/login'    , route_login.login);
app.get(webroot  + '/login/qr.png'    , route_login.qr);
app.get(webroot  + '/logout'   , route_main.logout);
app.post(webroot + '/login'   , route_login.login);
app.get(webroot  + '/poll'     , route_login.poll);
app.post(webroot + '/poll'    , route_login.poll);

http.createServer(app).listen(app.get('port'), function(){
  console.log('device auth sample application listening on port ' + app.get('port'));
});

function loginControl(req,res,next) {
	if (!req.session || !req.session.logged_in) {
	  res.redirect(webroot + '/login');
  } else {	
	  next();
  }
}
