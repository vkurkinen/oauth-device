Here's a quick guide how to contribute:

## Contribution Process

Fork-Pull Request model in use:

1. Fork the repo.

2. Run the tests. We only take pull requests with passing tests, and it's great
to know that you have a clean slate.

3. Add a test for your change. Only refactoring and documentation changes
require no new tests. If you are adding functionality or fixing a bug, there needs to a test!

4. Make the test pass.

5. Push to your fork and submit a pull request.

## Commit Message Formatting

For commit messages, use the conventional formatting described by [conventional-changelog](https://github.com/ajoslin/conventional-changelog/blob/master/CONVENTIONS.md).
