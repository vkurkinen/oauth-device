/**
* Client library for interacting with the Device OAuth Server Gateway.
*
* Provides functions to initiate a new device auth transaction, to poll
* authorization status based on the device code, and to refresh existing
* tokens with a refresh token from the specified IdP/AS.
*
* Application creates an instance of OAuth2DeviceClient and passes to it
* the URL to the Device Auth Gateway, identifier of the IdP to be used
* (must be known and registered to the Gateway), and OAuth2 client 
* credentials assigned to the application. 
* 
* Client credentials must be registered to the IdP/AS being used before hand
* as those are used by the Gateway when proxying requests from the device
* application to the AS.
*/
var request = require('request');
var util    = require("util");
var events  = require("events");

/**
*  Create new OAuth2DeviceClient with the specified settings.
*  @param idp_key The ID of the IdP known to the Gateway.
*/
function OAuth2DeviceClient(host, ns_key, idp_key,client_id, client_secret) {
	this.log = require('log4js').getLogger('oauth2_device_client');
	
	this.host          = host;
	this.code_url      = host + '/oauth/device/' + ns_key + '/' + idp_key + '/code';
	this.token_url     = host + '/oauth/device/' + ns_key + '/' + idp_key + '/token';
	this.client_id     = client_id;
	this.client_secret = client_secret;
	this.auth_creds = {
			user: client_id,
			pass: client_secret,
			sendImmediately: true
	}; 			
	events.EventEmitter.call(this);
}

util.inherits(OAuth2DeviceClient, events.EventEmitter);

/**
* Request new device auth transaction for the specified scope from the 
* IdP this client is associated with and for the client application 
* identified by the client_id set to this instance.
*
* Calls the provided callback function with the newly created 
* device auth entry when it is available.
*
*/
OAuth2DeviceClient.prototype.code = function (scope, lang, callback) {
    var body = { response_type : 'device_code', scope : scope};			  
    var code_url = this.code_url;
    if (lang) {
    	body.lang = lang;    
    }

    console.log("body -> " + JSON.stringify(body));

    var options = { strictSSL: false, followRedirect: true, form : body };
    if (this.auth_creds) {
    	options.auth = this.auth_creds;
    }    
    var self = this;
    request.post(code_url, options, function(err,resp,data) {    	
    	self.handleAuthResponse(null,err,resp,data,callback);
    });	
};

/**
* Check the status of an ongoing device auth transaction and get the 
* access tokens or error result in case the transaction has been 
* completed by the user. 
*
* Calls the provided callback when information is available.
* Callback gets the tokens/error response received from the 
* gateway or an "error" object indicating internal error.
*/
OAuth2DeviceClient.prototype.token = function (device_code, callback) {
	var body = { grant_type : 'device_code', device_code: device_code};			  
	var options = { strictSSL: false, followRedirect: true, form : body };
	if (this.auth_creds) {
    	options.auth = this.auth_creds;
    }
    var self = this;	
	request.post(this.token_url, options, function (err,resp,data) {
		self.handleAuthResponse(device_code, err,resp,data,callback);				
	});
};

/**
* Refresh tokens from the actual IdP via the Gateway with the given refresh token.
* Invokes the callback with new tokens or error when information is available.
*/
OAuth2DeviceClient.prototype.refresh = function (refresh_token, callback) {
	var body = { grant_type : 'refresh_token', refresh_token: refresh_token};			  
	var options = { strictSSL: false, followRedirect: true, form : body };
	if (this.auth_creds) {
    	options.auth = this.auth_creds;
    }
	var self = this;
	request.post(this.token_url, options, function (err,resp,data) {
		self.handleAuthResponse(refresh_token, err,resp,data,callback);				
	});
};

// handle response received from Gateway.
OAuth2DeviceClient.prototype.handleAuthResponse = function(event_ref, err,resp,data,callback) {	
	var error = null;
	if (err) {	    
		error = {
			error: 'server_error',  
			error_description: 'request to remote server failed'
		};
	} else {  		
		try {
			var data = JSON.parse(data);
			var error = null;
			if (data.error) {
				error = data;
				data = null;
			}
		} catch (e) {
			error = {
				error: 'server_error', 
				error_description: 'malformed data received from remote server: ' + e.message + ", data -> " + data
			};
		}		
	}

	// fire event from the current result.
	var e = {
		ref   : event_ref, 
		data  : data,
		error : error,
		type  : null
	};
	if (error) {
		e.type = error.error;
	} else {
		if (data.device_code) {
			e.type = 'device_code';
			e.ref = data.device_code;
		} else if (data.access_token) {
			e.type = 'tokens';
		} 
	}
		
	// always send common 'data' event to allow 'catch all' listeners
	this.emit('data', e);
	// then more fine grained event per type	
	this.emit(e.type, e);

	if (typeof callback == 'function') {
		callback(error, data);
	}
};

module.exports = OAuth2DeviceClient;
