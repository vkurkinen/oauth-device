OAuth2 Device Authorization Client
==================================

This module provides an easy to use client library for a node application to 
integrate to the [OAuth2 Device Authorization Gateway](../server/README.md). 

The library provides simple local functions with callbacks to be used instead of the 
actual [HTTP API](../server/doc/DEVICE_AUTH_API.md) and also triggers events of 
interesting situations to make client side logic even simpler. 

Creating Client Instance
------------------------

To take it into use, do: 

    var OAuth2DeviceClient = require('oauth2-device-client');
    var auth = new OAuth2DeviceClient(host, server_key,client_id, client_secret);
    
So create a new instance of the client with settings: 

   * `host`: Base URL of the OAuth2 Device Gateway (e.g. http://localhost:3000)
   * `ns_key`: Identifier of the namespace used by the authorization server for providing
      system / tenant or such specific UI theming and other configuration. This value depends 
      on the gateway configuration and must be provided by the gateway 
      deployment. A default namespace `d` should always be available.
   * `server_key`: Identifier of the authorization server to be used in the gateway. 
      This value depends on the gateway configuration and must be provided by the gateway 
      deployment.
   * `client_id`: Your OAuth2 client identifier in the target authorization server.
   * `client_secret`: Your OAuth2 client secret in the target authorization server.

After creating the instance you can start requesting authorizations from the targer
authorization server via the gateway.

The API is constructed of a set of main methods and a set of asynchronous events. Events
are optional to use so you can choose to ignore them and utilize only callback functions.

Each API call takes as an argument a callback function that gets called with 
two arguments `error` and `data`.

OAuth2 Device Client API
------------------------

The actual API operations are described below.

### code(scope, lang, callback)

Request new device auth transaction for the specified `scope` from the 
authorization server this client instance is associated with and for the 
client application identified by the `client_id` set to this instance.

Optionally, tells the device auth gateway a hint about the language that 
should be used when serving the user interface pages when user enters
to the verification URI to perform the authorization. If no language preference
exist, provide `null` as value for `lang`. Otherwise, use a language code supported
by the gateway.

Calls the provided `callback` function with the newly created device auth 
entry when it is available.

### token(device_code, callback)

Check the status of an ongoing device auth transaction identified with the 
provided `device_code` and get the access tokens or error result in case the 
transaction has been completed by the user. 

Calls the provided callback when information is available.
Callback gets the tokens/error response received from the 
gateway or an "error" object indicating internal error.

### refresh(refresh_token, callback)

Refresh tokens from the authorization server via the gateway with the given `refresh_token`.
Invokes the callback with new tokens or error when information is available.

## Example

Below code example shows the usage:

    var OAuth2DeviceClient = require('oauth2-device-client');
    var log = require('log4js').getLogger("auth");
    
    // our runtime settings
    var gateway_host  = "http://localhost:3000";
    var ns_key        = "d";
    var server_key    = "mock";
    var client_id     = "test";
    var client_secret = "test";
        
    // the authorization scopes we are requesting consent from user.
    var scope = "profile email phone";
    
    var auth = new OAuth2DeviceClient(gateway_host, ns_key, server_key,client_id, client_secret)

    // initiate new authorization transaction
    auth.code(scope, 'en', function (error, data) {
        if (error) {
            log.error("device authz request failed: " + JSON.stringify(error));
        } else {
            var verification_uri = data.verification_uri;
            var user_code        = data.user_code;
            var interval         = data.interval + 1;
            var expires_at       = new Date(new Date().getTime() + data.expires_in * 1000);
            var device_code      = data.device_code;
            // show verification_uri and user_code to user, 
            // store device code e.g. in server side session and start polling 
            // for tokens either in background or upon request
            // NOTE: you can also use 'device_code' event instead of callback function
        });
    };
    
    // later, poll for tokens
    auth.token(device_code, function(error, tokens) {
        if (error) {            
            switch (error.error) {
                case 'authorization_pending':
                case 'slow_down':
                    log.info("authorization still in progress...");
                    break;
                case 'invalid_grant':
                   log.warn("code seems expired or used");
                   break;
                default:
                    log.error("received error for login poll");
            }                       
            return;
        } 
        // else we have tokens
        var access_token = tokens.access_token;
        var refresh_token = tokens.refresh_token;
        
        // do something with the tokes.
    }
    
    // after access token has expired, get new ones
    auth.refresh(access_token function(error, tokens) {
        if (error) {            
            log.error("failed to refresh tokens, may be user has revoked our grant");
            // log user out and force re-authz process.
            return;
        }         
        var access_token = tokens.access_token;
        var refresh_token = tokens.refresh_token;
    }

In addition to the callback function, the client library emits events from all 
interesting situations.You can utilize events instead of the callback if needed.
    
Events
------

Eeach operation triggers an event from the client. Listeners can be added to 
the client as usual:

    client.on('data', mycallback);
    client.on('device_code', mycallback);
    client.on('tokens', mycallback);

Event listeners are invoked with an event object as argument. The event 
object is like:

    {
        "type":<event type>,
        "ref": <event reference>,
        "data": <event-data>
        "error": <error details>
    }

`type` is the identifier of the event, it has same value as the emitted event type.

`ref` contains the reference of the event i.e. the operations key parameter value 
 the event is related to. usually device code or access token (in case of refresh token operation).
 
`data` contains the data response of a successful operation, like token response. 
Data is not present if error is present.

`error` contains the error response of a failed operation. Usually the OAuth2 error 
response object (so it has attributes `error` and optionally `error_description`).

Below is an example of an event emitted when a new device authorization transaction is initiated:

    {
        "type":"device_code",
        "ref"   : "RRgeyJdKqTyQHGyiZEHaF0W3fN4o4rBy",
        "data"  : {
            "device_code":"RRgeyJdKqTyQHGyiZEHaF0W3fN4o4rBy",
            "user_code":"627967",
            "verification_uri":"http://localhost:3000/oauth/device/mock/user",
            "expires_in":300,
            "interval":1
        },
        "error":null,
    }

Another example when polling is done to a transaction that is still in progress:

    {
        "type":"authorization_pending",
        "ref"  : "RRgeyJdKqTyQHGyiZEHaF0W3fN4o4rBy",
        "data"  : null,
        "error" : {
            "error":"authorization_pending"
        }
    }

Finally, an example of an event that is triggered when authorization has been 
completed and tokens are available: 

    {
        "type":"tokens",
        "ref":"RRgeyJdKqTyQHGyiZEHaF0W3fN4o4rBy",
        "data": {
            "token_type":"Bearer",        
            "refresh_token":"nNTTCRWXl2ssuF786xcfEmuZwY7GrpjanSurR2K9z8",
            "access_token":"aIbH0imKX7yXtISTF1SP44lRbQqBiKDG"
        },
        "error":null
    }

The list of events triggered by the API closely follows the possible
error responses that may be returned by device gateway.

Following events are of most interest:

   * `device_code`: New device authorization transaction has been initiated.
   * `tokens`: Access tokens were received successfully.
   * `authorization_pending`: The authorization is still in progress.
   * `slow_down`: You are polling the gatewa too fast.
   * `access_denied`: The authorization was denied.
   * `server_error`: Something went terribly wrong.
   * `invalid_grant`: Provided code or token is wrong, expired or used (if one time code).

Use of events is optional, same information is available also in 
callback functions.

