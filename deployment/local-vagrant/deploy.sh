#!/bin/bash

VAULT_PASS_FILE="~/.o2dev-vault-pass"
VAULT_OPTS=""
if [ -r "${VAULT_PASS_FILE}" ]
then
   VAULT_OPTS="--vault-password-file=${VAULT_PASS_FILE}"  
else
   VAULT_OPTS="--ask-vault-pass"
fi

ansible-playbook ../ansible/site.yaml $VAULT_OPTS -i development -s -f 1