#!/bin/bash

#
# Helper to create initia security group to AWS. Not complete.
#

aws ec2 create-security-group --group-name oauth2-device-dev --description "OAuth2 Device Development & Testing"

# rules:
aws ec2 authorize-security-group-ingress --group-name oauth2-device-dev --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name oauth2-device-dev --protocol tcp --port 3000 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name oauth2-device-dev --protocol tcp --port 3001 --cidr 0.0.0.0/0