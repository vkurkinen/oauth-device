#!/bin/bash

. ~/bin/setup_aws_env.sh 

ansible tag_o2dev_group_appservers -i ec2.py -u ubuntu -m command -a "pm2 restart all"