#!/bin/bash

#
# Dump EC2 facts from the relevant nodes.
#
ansible tag_Service_o2dev -i ec2.py -s -f 1 -u ubuntu  -m ec2_facts 