#!/bin/bash

#
# Deploy the API and demo into existing nodes in AWS. 
# Nodes are detected based on the tags associagted with the nodes.
# See: create-instances playbook.
#


VAULT_PASS_FILE="~/.o2dev-vault-pass"
VAULT_OPTS=""
if [ -r "${VAULT_PASS_FILE}" ]
then
   VAULT_OPTS="--vault-password-file=${VAULT_PASS_FILE}"  
else
   VAULT_OPTS="--ask-vault-pass"
fi

ansible-playbook ../ansible/aws-site.yaml $VAULT_OPTS -i ec2.py -s -f 1 -u ubuntu -e "@../ansible/vars/aws_conf.yml"