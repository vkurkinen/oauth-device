#!/bin/bash

#
# Sets the AWS keys into environment where Ansible expects them to be. 
#
# This assumes that the keys are in your home directory in .aws/config i.e. 
# created with 'aws configure'...
# 

AWS_ACCESS_KEY_ID=`grep "aws_access_key_id" ~/.aws/config | cut -d=  -f 2 | tr -d ' '`
AWS_SECRET_ACCESS_KEY=`grep "aws_secret_access_key" ~/.aws/config | cut -d=  -f 2 | tr -d ' '`

export AWS_SECRET_ACCESS_KEY
export AWS_ACCESS_KEY_ID

